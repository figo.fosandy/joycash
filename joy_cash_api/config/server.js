'use strict'
const Hapi = require('@hapi/hapi')

require('dotenv').config()
const config = require('./config.json')[process.env.NODE_ENV.toLowerCase()];
const {connectMongo} = require('./connectMongo')
const route = require('../routes')


const server = new Hapi.server({
    port: config.port,
    host: "0.0.0.0",
})

connectMongo()

server.route(route)

const io = require('socket.io')(server.listener)

server.method('emit',(event, data) => {
    io.emit(event, data);
},{})

exports.start = async () => {
    await server.start()
    console.log('Server running at: ', server.info.uri)
    return server
}


exports.init = async () => {
    await server.initialize();
    return server;
}

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
})