const mongoose = require('mongoose')
require('dotenv').config()
const config = require('./config.json')[process.env.NODE_ENV.toLowerCase()];

const connectMongo = async () => {
    try {
        await mongoose.connect(config.mongoUrl, {
            useNewUrlParser : true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })
        console.log(`MongoDB connect at : ${config.mongoUrl}`)
        mongoose.set('debug', true)
    }catch{
        process.exit(1)
    }
}

module.exports = {connectMongo}