const Mongooose = require('mongoose')
const { Schema } = Mongooose

const userSchema = new Schema({
    address : {
        type : String,
        required : [true, 'address is required'],
        default : "address is empty"
    },
    firstName : {
        type : String,
        required : [true, 'firstname is required'],
        default : "firstname is empty"
    },
    lastName : {
        type : String,
        required : [true, 'lastname is required'],
        default : "lastname is empty"
    },
    uniqueId : {
        type : String,
        unique : true,
        required : [true, 'unique Id is required'],
        default : Date.now
    },
    email: {
        type: String,
        required: [true, 'Email id is Required'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'password is required']
    },
    locked: {
        type: Number,
        default: Date.now
    },
    countFailed: {
        type: Number,
        default: 0
    },
    pin: String,
    amount: {
        type: Number,
        default: 0
    },
    payees: {
        type: [String],
        default: []
    }
})

const transactionSchema = new Schema ({
    email: {
        type: String,
        required: [true, 'email is required']
    },
    type: {
        type: String,
        required: [true, 'type is required']
    },
    date: {
        type: Number,
        default: Date.now
    },
    amount: {
        type: Number,
        required: [true, 'amount is required']
    },
    fromTo: String,
    category: String,
    description: String
})

const requestSchema = new Schema({
    from: {
        type: String,
        required: [true, 'email from is required']
    },
    to: {
        type: String,
        required: [true, 'email to is required']
    },
    date: {
        type: Number,
        default: Date.now
    },
    amount: {
        type: Number,
        required: [true, 'amount is required']
    },
    description: {
        type: String,
        required: [true, 'description is required']
    },
    status: {
        type: String,
        default: 'pending'
    }
})

userSchema.pre('findOneAndUpdate', async function() {
    const docToUpdate = await this.findOne(this.getQuery())
    if (docToUpdate != null && docToUpdate.countFailed > 1) {
        this.update({},{countFailed:0, locked: Date.now() + 900000})
    }
})

const User = Mongooose.model('user', userSchema)
const Transaction = Mongooose.model('transaction', transactionSchema)
const Request = Mongooose.model('request', requestSchema)

module.exports = { User, Transaction, Request }