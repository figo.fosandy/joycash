const Lab = require('@hapi/lab');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { expect } = require('@hapi/code')
const { init } = require('../config/server');

describe('GET /', () => {
    let server

    beforeEach(async () => {
        server = await init();
    })

    afterEach(async () => {
        await server.stop();
    })

    it(' see all data ', async () => {
        const res = await server.inject({
            method: 'GET',
            url: '/'
        });
        expect(res.statusCode).to.equal(200);
    })

    it('Test for Pin ', async () => {
        const email = `user${Date.now()}@mail.com`
        await server.inject({
            method: 'POST',
            url: '/user/register',
            payload: {
                email,
                password: 'password'
            }
        })

        const res = await server.inject({
            method: 'PUT',
            url: `/user/pin/${email}`,
            payload: {
                pin: '12rt'
            }
        })

        expect(res.statusCode).to.equal(202)
    })

    it('test for AddPayee', async () => {
        const email = `user${Date.now()}@mail.com`
        await server.inject({
            method: 'POST',
            url: '/user/register',
            payload: {
                email,
                password: 'password'
            }
        })
        const res = await server.inject({
            method: 'PUT',
            url: `/user/addPayee/${email}`,
            payload: {
                email: 'lala@mail.com'
            }
        });
        expect(res.statusCode).to.equal(202)
    })


    it(' Test for Register ', async () => {
        const email = `user${Date.now()}@mail.com`
        const res = await server.inject({
            method: 'POST',
            url: '/user/register',
            payload: {
                email: email,
                password: 'joycash123'
            }
        });
        expect(res.statusCode).to.equal(201)
    })

    it(' Test for Setting Up profiles ', async () => {
        const email = `user${Date.now()}@mail.com`
        await server.inject({
            method: 'POST',
            url: '/user/register',
            payload: {
                email: email,
                password: 'joycash123'
            }
        });
    
        const res = await server.inject({
            method: 'PUT',
            url: `/user/profile/${email}`,
            payload: {
                firstName: 'user',
                lastName: 'unique',
                uniqueId: email.split('@')[0],
                address: 'bali',
            }
        });
        expect(res.statusCode).to.equal(201)
    })

    it(' Test for Login ', async () => {
        const email = `user${Date.now()}@mail.com`
        await server.inject({
            method: 'POST',
            url: '/user/register',
            payload: {
                email: email,
                password: 'joycash123',
                uniqueId: email.split('@')[0]
            }
        });
        
        const res = await server.inject({
            method: 'POST',
            url: '/user/login',
            payload: {
                email: email,
                password: 'joycash123'
            }
        });

        const uniqueIdRes = await server.inject({
            method: 'POST',
            url: '/user/login',
            payload: {
                uniqueId: email.split('@')[0],
                password: 'joycash123'
            }
        });

        expect(res.statusCode).to.equal(202)
        expect(uniqueIdRes.statusCode).to.equal(202)
    })

    it(' Test for Load Transaction ', async () => {
        const email = `user${Date.now()}@mail.com`
        const res = await server.inject({
            method: 'GET',
            url: `/user/transaction/${email}`,
        });
        expect(res.statusCode).to.equal(200)
    })

    it('Test for Payment', async () => {
        const randomNumber = Date.now()
        const from = `user${randomNumber}@mail.com`
        const to = `user${randomNumber + 1}@mail.com`
        const password = 'password'
        const pin = 'pin1'
        await server.inject({
            method: 'POST',
            url: '/user/register',
            payload: {
                email: from,
                password,
                pin,
                amount: 10000
            }
        })
        await server.inject({
            method: 'POST',
            url: '/user/register',
            payload: {
                email: to,
                password,
                pin,
                amount: 10000
            }
        })
        const res = await server.inject({
            method: 'POST',
            url: '/user/payment',
            payload: {
                from,
                to,
                pin,
                amount: 5000,
                description: 'testing',
                category: 'food'
            }
        })
        expect(res.statusCode).to.equal(202)
        const lastBalanceFrom = await server.inject({
            method: 'GET',
            url: `/user/amount/${from}`
        })
        const lastBalanceTo = await server.inject({
            method: 'GET',
            url: `/user/amount/${to}`
        })
        expect(lastBalanceFrom.result.amount).to.equal(5000)
        expect(lastBalanceTo.result.amount).to.equal(15000)
    })
})