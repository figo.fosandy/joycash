const {start} = require('./config/server')

start().catch((err) => {
    console.log(err)
    process.exit(1)
})