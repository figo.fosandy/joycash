const {
    registerHandler, 
    loginHandler, 
    profileHandler, 
    editPinHandler, 
    loadUserListHandler, 
    paymentHandler, 
    loadTransactionListHandler, 
    addPayeeHandler, 
    topupHandler, 
    verifyHandler, 
    getAmountHandler, 
    requestPaymentHandler,
    responsePaymentHandler,
    loadRequestHandler,
    getPayeesHandler
} = require('../handler')

const root = {
    method: 'GET',
    path: '/',
    handler: (request, h) => {
        return h.response({message : "test123"})
    }
}

const register = {
    method: 'POST',
    path: '/user/register',
    handler: registerHandler
}

const login = {
    method: 'POST',
    path: '/user/login',
    handler: loginHandler
}

const profile = {
    method: 'PUT',
    path: '/user/profile/{email}',
    handler: profileHandler
}

const pinEdit = {
    method: 'PUT',
    path: '/user/pin/{email}',
    handler: editPinHandler
}

const loadUserList = {
    method: 'GET',
    path: '/user/list',
    handler: loadUserListHandler
}

const topup = {
    method: 'POST',
    path: '/user/topup/{email}',
    handler: topupHandler
}

const verifytopup = {
    method: 'POST',
    path: '/user/verifytopup/{email}/{token}',
    handler: verifyHandler
}


const payment = {
    method: 'POST',
    path: '/user/payment',
    handler: paymentHandler
}

const loadTransactionList = {
    method: 'GET',
    path: '/user/transaction/{email}',
    handler: loadTransactionListHandler
}

const addPayee = {
    method: 'PUT',
    path: '/user/addPayee/{email}',
    handler: addPayeeHandler
}

const getAmount = {
    method: 'GET',
    path: '/user/amount/{email}',
    handler: getAmountHandler
}

const getPayees = {
    method: 'GET',
    path: '/user/payees/{email}',
    handler: getPayeesHandler
}

const requestPayment = {
    method: 'POST',
    path: '/user/request',
    handler: requestPaymentHandler
}

const responsePayment = {
    method: 'POST',
    path: '/user/response/{id}',
    handler: responsePaymentHandler
}

const loadRequest = {
    method: 'GET',
    path: '/user/request_list/{email}',
    handler: loadRequestHandler
}

module.exports = [
    root, 
    register, 
    login, 
    profile, 
    pinEdit, 
    loadUserList, 
    payment, 
    loadTransactionList, 
    addPayee, 
    topup, 
    verifytopup, 
    getAmount, 
    requestPayment,
    responsePayment,
    loadRequest,
    getPayees
]