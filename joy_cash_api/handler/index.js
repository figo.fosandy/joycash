const Boom = require('@hapi/boom')
const Axios = require('axios')
const {User, Transaction, Request} = require('../config/mongoSchema')

exports.registerHandler = (request, h) => {
    const {payload} = request
    return User.create(payload)
    .then(res => h.response(res).code(201))
    .catch(error => {
        const errorMessage = error.toString()
        if (errorMessage.match(/ValidationError|CastError/)) {
            return Boom.badRequest("invalid email id or password")    
        } else if(errorMessage.match('E11000')) {
            return Boom.conflict("email is already use")
        } else {
            return Boom.boomify(error)
        }
    })
}

exports.loginHandler = (request, h) => {
    const {payload} = request
    const data = payload.email ? {email: payload.email} : {uniqueId: payload.uniqueId}
    return User.findOne(data).lean()
        .then(async res => {
            if (!res) {
                return Boom.notFound('No User found');
            }
            if (res.locked > Date.now()) {
                return Boom.notAcceptable('This account is locked for 15 minutes')
            }
            if (res.password != payload.password) {
                await User.findOneAndUpdate(data, {countFailed: res.countFailed + 1}).lean()
                return Boom.notAcceptable('Wrong Password');
            }
            await User.findOneAndUpdate(data, {countFailed: 0}).lean()
            return h.response(res).code(202)
        })
        .catch(error => Boom.boomify(error))
}

exports.profileHandler = (request, h) => {
    const {email} = request.params
    const {payload} = request
    return User.findOneAndUpdate({"email" : email}, payload, {new: true}).lean()
    .then( res => h.response(res).code(201))
    .catch(error => {
        const errorMessage = error.toString()
        if (errorMessage.match(/ValidationError|CastError/)) {
            return Boom.badRequest("invalid character for firstname or password")    
        } else if(errorMessage.match('E11000')) {
            return Boom.conflict("uniqueId is already use")
        } else {
            return Boom.boomify(error)
        }    
    })
}

exports.editPinHandler = (request, h) => {
    const {email} = request.params
    const {payload} = request
    return User.findOneAndUpdate({'email': email}, payload, {new: true}).lean()
        .then(res => {
            if(! res){
                return Boom.notFound('email not found')
            }
            return h.response(res).code(202)
        }) 
        .catch(error => Boom.boomify(error))
}

exports.topupHandler = (request, h) => {
    const {email} = request.params
    const { payload } = request
    return Axios.post("https://ewalletapis.herokuapp.com/topup",{
        "amount" : payload.amount,
        "useremail" : email
    })
    .then( res => {
            if(!res){
                return Boom.unauthorized('email not registered')
            }
            return h.response(res.data).code(200)
    })
    .catch(error => {
        return Boom.notAcceptable(error.response.data.message)
    })
}

exports.verifyHandler = (request, h) => {
    const { token, email } = request.params
    const { payload } = request
    return Axios.post("https://ewalletapis.herokuapp.com/verifytopup", {
        "token" : token,
        "pin" : payload.pin
    })
    .then( res => {
            if(!res.data.success){
                return Boom.failedDependency('pin not valid')
            }
            return User.findOneAndUpdate({email}, {$inc: {amount: parseInt(payload.amount)}}).lean()
                    .then(res => {
                        if(!res){
                            return Boom.notFound("User not found")
                        }
                        return Transaction.create({
                                email,
                                type: 'topup',
                                amount: parseInt(payload.amount)
                        })
                            .then(res => h.response(res))
                            .catch(error => {
                                return error.toString()
                            })
                    })

        })
        .catch(error => {
            return Boom.boomify(error)
        }) 
}

exports.loadUserListHandler = (request, h) => {
    return User.find({}, {email: 1, firstName: 1, lastName: 1, uniqueId: 1}).lean()
        .then(async res => {
            return h.response(res).code(200)
        })
        .catch(error => Boom.boomify(error))
}

exports.paymentHandler = (request, h) => {
    const {payload} = request
    return User.findOne({email: payload.from}).lean()
        .then(async res => {
            if (! res) {
                return Boom.notFound('Sender email id not found')
            }
            if (res.pin != payload.pin) {
                return Boom.notAcceptable('Wrong pin')
            }
            if (payload.amount > res.amount) {
                return Boom.notAcceptable('Insufficent ammount')
            }
            return User.findOne({email: payload.to})
            .then(async res => {
                    if(! res) {
                        return Boom.notFound('Receiver email id not found')
                    }
                    await User.findOneAndUpdate({email: payload.from}, {$inc: {amount: -1 * parseInt(payload.amount)}}).lean()
                    await User.findOneAndUpdate({email: payload.to}, {$inc: {amount: parseInt(payload.amount)}}).lean()
                    await Transaction.create({
                        email: payload.to,
                        type: 'credit',
                        fromTo: payload.from,
                        amount: parseInt(payload.amount),
                        description: payload.description
                    })
                    return Transaction.create({
                        email: payload.from,
                        type: 'debit',
                        fromTo: payload.to,
                        amount: -1 * parseInt(payload.amount),
                        category: payload.category,
                        description: payload.description
                    })
                        .then(async res => { 
                            request.server.methods.emit('update',payload.to)
                            return h.response(res).code(202)
                        })
                        .catch(error => Boom.boomify(error))
                })
                .catch(error => Boom.boomify(error))
        })
        .catch(error => Boom.boomify(error))
}

exports.loadTransactionListHandler = (request, h) => {
    const {params} = request
    return Transaction.find({email: params.email}).lean().sort({date: -1}).lean()
        .then(async res => {
            return h.response(res).code(200)
        })
        .catch(error => Boom.boomify(error))
}

exports.addPayeeHandler = (req, h) => {
    const {email} = req.params
    const {payload} = req
    return User.findOneAndUpdate({'email': email}, {$push: {payees: payload.email}}, {new: true}).lean()
        .then(res => {
            if(! res){
                return Boom.notFound('email not found')
            }
            return h.response(res).code(202)
        }) 
}

exports.getAmountHandler = (request, h) => {
    const {params} = request
    return User.findOne(params, {amount: 1}).lean()
        .then(res => {
            if(! res){
                return Boom.notFound('email not found')
            }
            return h.response(res).code(200)
        })
}

exports.getPayeesHandler = (request, h) => {
    const {params} = request
    return User.findOne(params, {payees: 1}).lean()
        .then(res => {
            if(! res){
                return Boom.notFound('email not found')
            }
            return h.response(res).code(200)
        })
}

exports.requestPaymentHandler = (request, h) => {
    const {payload} = request
    return Request.create(payload)
        .then(res => {
            return User.findOneAndUpdate({email: payload.to, payees: {$nin:[payload.from]}}, {$push: {payees: payload.from}}).lean()
                .then(() => {
                    request.server.methods.emit('request',payload.to)
                    return h.response(res).code(201)
                })
        })
        .catch(error => {
            const errorMessage = error.toString()
            if (errorMessage.match(/ValidationError|CastError/)) {
                return Boom.badRequest("invalid input")    
            } 
            return Boom.boomify(error)
        })
}

exports.responsePaymentHandler = (request, h) => {
    const {params} = request
    return Request.findByIdAndUpdate(params.id, {status: 'paid', date: Date.now()}, {new: true}).lean()
    .then(res => {
        request.server.methods.emit('request', res.from)
        return h.response(res).code(202)
    })
    .catch(err => Boom.boomify(err))
}

exports.loadRequestHandler = (request, h) => {
    const {params} = request
    return Request.find({
        $or: [{
            from: params.email
        }, {
            to: params.email
        }]
    }).lean().sort({status: -1, date: -1})
        .then(res => {
            const result = res.map(eachRequest => {
                return {...eachRequest, id: eachRequest._id}
            })
            return h.response(result).code(201)
        })
        .catch(err => Boom.boomify(err))
}