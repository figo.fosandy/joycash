import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/screen/add_payee.dart';
import 'package:joy_cash_app/screen/detail_transaction.dart';
import 'package:joy_cash_app/screen/landingPage.dart';
import 'package:joy_cash_app/screen/login.dart';
import 'package:joy_cash_app/screen/payment.dart';
import 'package:joy_cash_app/screen/profileCreation.dart';
import 'package:joy_cash_app/screen/register.dart';
import 'package:joy_cash_app/screen/request.dart';
import 'package:joy_cash_app/screen/request_history.dart';
import 'package:joy_cash_app/screen/response_history.dart';
import 'package:joy_cash_app/screen/set_pin.dart';
import 'package:joy_cash_app/screen/dashboard.dart';
import 'package:joy_cash_app/screen/topup.dart';
import 'package:joy_cash_app/screen/transaction_history.dart';
import 'package:redux/redux.dart';

class App extends StatelessWidget {
  final Store<AppState> store;

  const App({Key key, this.store}) : super(key: key);

  Widget _appChild() {
    return MaterialApp(
      title: 'Joy Cash',
      initialRoute: '/landingPage',
      theme: ThemeData.dark(),
      onGenerateRoute: _getRoute,
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: _appChild()
    );
  }
}

Route<dynamic> _getRoute(RouteSettings settings) {
  switch (settings.name) {
    case '/landingPage' :
      return MaterialPageRoute(builder: (_) => Scaffold(body: LandingPage()));
    case '/register':
      return MaterialPageRoute(builder: (_) => Scaffold(body: RegisterPage()));
    case '/login':
      return MaterialPageRoute(builder:  (_) => Scaffold(body: LoginPage()));
    case '/profile':
      return MaterialPageRoute(builder:  (_) => Scaffold(body: ProfilePage()));
    case '/pin':
      return MaterialPageRoute(builder: (_) => SetPinPage());
    case '/dashboard':
      return MaterialPageRoute(builder:  (_) => Scaffold(body: HomeScreen()));
    case '/topup':
      return MaterialPageRoute(builder:  (_) => Scaffold(body: TopupPage()));
    case '/payment':
      return MaterialPageRoute(builder: (_) => Scaffold(body: PaymentPage()));
    case '/payee':
      return MaterialPageRoute(builder:  (_) => Scaffold(body: AddPayeePage()));
    case '/transactionHistory':
      return MaterialPageRoute(builder:  (_) => Scaffold(body: TransactionLog()));
    case '/detailTransaction':
      return MaterialPageRoute(builder: (_) => Scaffold(body: DetailTransactionPage(settings.arguments)));
    case '/request':
      return MaterialPageRoute(builder: (_) => Scaffold(body: RequestPage()));
    case '/requestHistory':
      return MaterialPageRoute(builder: (_) => Scaffold(
        body: RequestHistoryPage(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(_).pushNamed('/request');
          },
          child: Icon(Icons.add)
        ),
      ));
    case '/responseHistory':
      return MaterialPageRoute(builder: (_) => Scaffold(body: ResponseHistoryPage()));
    default:
      return null;
  }
}