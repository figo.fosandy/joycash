import 'package:flutter/material.dart';
import 'package:joy_cash_app/app.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/actions/load_amount.dart';
import 'package:joy_cash_app/redux/actions/load_payees.dart';
import 'package:joy_cash_app/redux/actions/load_user_list.dart';
import 'package:joy_cash_app/redux/actions/loading_request.dart';
import 'package:joy_cash_app/redux/actions/loading_transaction.dart';
import 'package:joy_cash_app/redux/reducers/reducers.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final persistor = Persistor<AppState>(
    storage: FlutterStorage(),
    serializer: JsonSerializer<AppState>(AppState.fromJson)
  );

  final initialState = await persistor.load();


  final store = Store<AppState>(
      appStateReducer,
      initialState: initialState,
      middleware: [persistor.createMiddleware(), thunkMiddleware, LoggingMiddleware.printer()]
  );

  store.dispatch(loadUserList());

  String uri = await loadAsset();

  IO.Socket socket = IO.io(uri, <String, dynamic> {
    'transports': ['websocket']
  });
  socket.on('update', (email) => {
    if (email == store.state.detailUser.email) {
      store.dispatch(getAmount()),
      store.dispatch(loadTransaction())
    }
  });
  socket.on('request', (email) => {
    if (email == store.state.detailUser.email) {
      store.dispatch(getPayees()),
      store.dispatch(loadRequest())
    }
  });

  return runApp(App(store: store));
}