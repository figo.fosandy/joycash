class Request{
  final String id;
  final String from;
  final String to;
  final String status;
  final int date;
  final int amount;
  final String description;

  Request({this.id, this.from, this.to, this.date, this.status, this.amount, this.description});

  Request copyWith({
    String id,
    String from,
    String to,
    String status,
    int date,
    int amount,
    String description,
  }) => Request(
    id: id ?? this.id,
    from: from ?? this.from,
    to: to ?? this.to,
    status: status ?? this.status,
    date: date ?? this.date,
    amount: amount ?? this.amount,
    description: description ?? this.description,
  );

  Request.fromJson(Map json)
    : id = json['id'],
      from = json['from'],
      to = json['to'],
      status = json['status'],
      date = json['date'],
      amount = json['amount'],
      description = json['description'];

  Map toJson() => {
    'id': id,
    'from':from,
    'to': to,
    'status': status,
    'date': date,
    'amount':amount,
    'description' : description
  };

  @override
  String toString() {
    return toJson().toString();
  }
}