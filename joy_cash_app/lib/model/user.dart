class User{
  final String email;
  final String firstName;
  final String lastName;
  final String address;
  final String uniqueId;
  final String pin;
  final int amount;
  final List<String> payees;
  final String token;

  User({this.email, this.address, this.firstName, this.lastName, this.uniqueId, this.pin, this.amount, this.payees, this.token});

  User copyWith({
    String email,
    String address,
    String firstName,
    String lastName,
    String uniqueId,
    String pin,
    int amount,
    List<String> payees,
    String token
  }) => User(
    email: email ?? this.email,
    address: address ?? this.address,
    firstName: firstName ?? this.firstName,
    lastName: lastName ?? this.lastName,
    uniqueId: uniqueId ?? this.uniqueId,
    pin: pin ?? this.pin ,
    amount: amount ?? this.amount,
    payees: payees ?? this.payees,
    token: token ?? this.token
  );

  User.fromJson(Map json)
    : email = json['email'],
      address = json['address'],
      firstName = json['firstName'],
      lastName = json['lastName'],
      uniqueId = json['uniqueId'],
      pin = json['pin'],
      amount = json['amount'],
      payees = json['payees'] == null ? [] : List<String>.from(json['payees']),
      token = json['token'];

  Map toJson() => {
    'email': email,
    'address': address,
    'firstName': firstName,
    'lastName': lastName,
    'uniqueId': uniqueId,
    'pin': pin,
    'amount': amount,
    'payees': payees,
    'token' : token
  };

  @override
  String toString() {
    return toJson().toString();
  }
}