class Transaction{
  final String email;
  final String fromTo;
  final String type;
  final int amount;
  final int date;
  final String description;
  final String category;

  Transaction({this.email, this.fromTo, this.type, this.amount, this.date, this.description, this.category});

  Transaction copyWith({
    String email,
    String fromTo,
    String type,
    int amount,
    int date,
    String description,
    String category
  }) => Transaction(
    email: email ?? this.email,
    fromTo: fromTo ?? this.fromTo,
    type: type ?? this.type,
    amount: amount ?? this.amount,
    date: date ?? this.date,
    description: description ?? this.description,
    category: category ?? this.category
  );

  Transaction.fromJson(Map json)
    : email = json['email'],
      fromTo = json['fromTo'],
      type = json['type'],
      amount = json['amount'],
      date = json['date'],
      description = json['description'],
      category = json['category'];


  Map toJson() => {
    'email': email,
    'fromTo':fromTo,
    'type': type,
    'amount':amount,
    'date': date,
    'description' : description,
    'category' : category
  };

  @override
  String toString() {
    return toJson().toString();
  }
}