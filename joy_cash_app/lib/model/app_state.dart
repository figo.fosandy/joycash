import 'package:joy_cash_app/model/request.dart';
import 'package:joy_cash_app/model/transaction.dart';
import 'package:joy_cash_app/model/user.dart';

class AppState{
  final User detailUser;
  final bool userLoading;
  final String failLogin;
  final bool isLogin;
  final bool transactionLoading;
  final String requestId;
  final String status;
  final String topupErrorMessages;
  final String verifyErrorMessages;
  final String paymentErrorMessages;
  final String requestErrorMessages;
  final List<User> userList;
  final List<Transaction> transactionList;
  final List<Request> requestList;
  

  AppState({this.detailUser, this.userLoading, this.failLogin, this.isLogin, this.userList, this.status, this.transactionLoading, this.topupErrorMessages, this.verifyErrorMessages, this.paymentErrorMessages, this.transactionList, this.requestErrorMessages, this.requestList, this.requestId});

  AppState.initialState() : 
    detailUser = User(),
    userLoading = false,
    failLogin = null,
    isLogin = false,
    status = null,
    transactionLoading = false,
    requestId = "",
    topupErrorMessages = "",
    verifyErrorMessages = "",
    paymentErrorMessages = "",
    requestErrorMessages = "",
    userList = List.unmodifiable(<User>[]),
    transactionList = List.unmodifiable(<Transaction>[]),
    requestList = List.unmodifiable(<Request>[]);
   
  AppState copyWith({
    User detailUser,
    bool userLoading,
    String failLogin,
    bool isLogin,
    bool transactionLoading,
    String topupErrorMessages,
    String verifyErrorMessages,
    String paymentErrorMessages,
    String requestErrorMessages,
    String requestId,
    String status,
    List<User> userList,
    List<Transaction> transactionList,
    List<Request> requestList
  }) => AppState(
    detailUser: detailUser ?? this.detailUser,
    userLoading: userLoading ?? this.userLoading,
    failLogin: failLogin ?? this.failLogin,
    isLogin: isLogin ?? this.isLogin,
    transactionLoading: transactionLoading ?? this.transactionLoading,
    topupErrorMessages:  topupErrorMessages ?? this.topupErrorMessages,
    verifyErrorMessages: verifyErrorMessages ?? this.verifyErrorMessages,
    paymentErrorMessages: paymentErrorMessages ?? this.paymentErrorMessages,
    requestErrorMessages: requestErrorMessages ?? this.requestErrorMessages,
    status: status ?? this.status,
    requestId: requestId ?? this.requestId,
    userList: userList ?? this.userList,
    transactionList: transactionList ?? this.transactionList,
    requestList: requestList ?? this.requestList
  );

  static AppState fromJson(dynamic json) {
    if (json == null) {
      return AppState.initialState();
    }
    return AppState(
      detailUser: User.fromJson(json['detailUser']),
      userLoading: json['userLoading'] as bool,
      failLogin: json['failLogin'],
      transactionLoading: json['transactionLoading'] as bool,
      topupErrorMessages: json['topupErrorMessages'],
      verifyErrorMessages: json['verifyErrorMessages'],
      paymentErrorMessages: json['paymentErrorMessages'],
      requestErrorMessages: json['requestErrorMessages'],
      isLogin: json['isLogin'] as bool,
      status: json['status'],
      requestId: json['requestId'] ?? '',
      userList: json['userList'] == null ? List.unmodifiable(<User>[]) : List.from(json['userList']).map((user) => User.fromJson(user)).toList(),
      transactionList: json['transactionList'] == null ? List.unmodifiable(<Transaction>[]) : List.from(json['transactionList']).map((transaction) => Transaction.fromJson(transaction)).toList(),
      requestList: json['requestList'] == null ? List.unmodifiable(<Request>[]) : List.from(json['requestList']).map((request) => Request.fromJson(request)).toList(),
    );
  }

  dynamic toJson() => {
    'detailUser': detailUser,
    'userLoading': userLoading,
    'failLogin':failLogin,
    'isLogin': isLogin,
    'status' : status,
    'requestId' : requestId,
    'topupErrorMessages' : topupErrorMessages,
    'verifyErrorMessages' : verifyErrorMessages,
    'paymentErrorMessages' :paymentErrorMessages,
    'requestErrorMessages' :requestErrorMessages,
    'transactionLoading' : transactionLoading,
    'userList': userList,
    'transactionList': transactionList,
    'requestList': requestList
  };

  @override
  String toString() {
    return toJson().toString();
  }
}