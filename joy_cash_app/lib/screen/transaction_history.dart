import 'package:flutter/material.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/transaction.dart';
import 'package:joy_cash_app/redux/actions/loading_transaction.dart';
import 'package:redux/redux.dart';

class TransactionLog extends StatefulWidget {
  TransactionLog({Key key}) : super(key: key);
  @override
  TransactionLogState createState() => TransactionLogState();
}


class TransactionLogState extends State<TransactionLog> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  int _sortValue = DateTime.now().year;
  String _filterValue;
  String _category;
  List<int> _dateRange = [];
  double _starValue = 0;
  double _endValue = 100000;

  // bool _autoValidate = true;
  String _searchValue = '';
  bool _seeFilter = false;
  bool _seeSearch = false;

  @override
  Widget build(BuildContext context) { 
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) =>
          _bodyTransactions(viewModel),
    );
  }

  ListView _buildTransactionsList(
      List<Transaction> list, context, _ViewModel model) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: list.length,
        padding: EdgeInsets.all(0),
        controller: ScrollController(keepScrollOffset: false),
        itemBuilder: (context, index) {
          return GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/detailTransaction',
                    arguments: list[index]);
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 32, vertical: 5),
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey[300],
                          blurRadius: 10.0,
                          spreadRadius: 4.5)
                    ]),
                child: Row(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          color: list[index].amount < 0 ? Colors.redAccent : Colors.lightGreen,
                          borderRadius: BorderRadius.all(Radius.circular(18))),
                      child: Icon(
                        list[index].amount < 0 ? Icons.call_made : list[index].type == "topup" ? Icons.loupe : Icons.call_received,
                      ),
                      padding: EdgeInsets.all(12),
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            list[index].type.toUpperCase(),
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                color: Colors.grey[900]),
                          ),
                          Text(
                            list[index].fromTo != null ? model.appState.userList.firstWhere((user) => user.email == list[index].fromTo).uniqueId : '',
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w700,
                                color: Colors.grey[500]),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          FlutterMoneyFormatter(
                            amount: list[index].amount == null ? 0 : list[index].amount.abs().toDouble(),
                            settings: MoneyFormatterSettings(
                              symbol: '${list[index].amount.isNegative ? '-' : '+'} IDR'
                            ),
                          ).output.compactSymbolOnLeft,
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              color: list[index].amount < 0 ? Colors.redAccent : Colors.lightGreen,)
                        ),
                        Text(
                          DateTime.fromMillisecondsSinceEpoch(list[index].date)
                              .toString()
                              .substring(0, 10),
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w700,
                              color: Colors.grey[500]),
                        ),
                      ],
                    ),
                  ],
                ),
              ));
        });
  }

  Widget _selectFilter(_ViewModel model) {
    return Container(
        child: Column(
      children: <Widget>[
        Container(
            margin: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 2),
                color: Colors.transparent,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(children: <Widget>[
              Text(
                "Select Payees",
                style: TextStyle(color: Colors.grey[500]),
              ),
              DropdownButtonFormField<String>(
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 30),
                    filled: _filterValue != null),
                value: _filterValue,
                icon: Icon(
                  Icons.account_circle,
                  color: Colors.grey,
                ),
                iconSize: 35,
                elevation: 16,
                style: TextStyle(
                    color: Colors.grey[500],
                    fontSize: 10,
                    fontWeight: FontWeight.bold),
                onChanged: (newValue) {
                  setState(() {
                    _filterValue = newValue;
                  });
                },
                items: <String>[...model.appState.detailUser.payees]
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ])),
        SizedBox(
          height: 10,
        ),
        Container(
            margin: EdgeInsets.symmetric(horizontal: 50),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 2),
                color: Colors.transparent,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              children: <Widget>[
                Text(
                  "Select Category",
                  style: TextStyle(color: Colors.grey[500]),
                ),
                DropdownButtonFormField<String>(
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(horizontal: 30),
                      filled: _category != null),
                  value: _category,
                  icon: Icon(
                    Icons.category,
                    color: Colors.grey,
                  ),
                  iconSize: 35,
                  elevation: 16,
                  style: TextStyle(
                      color: Colors.grey[500],
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                  onChanged: (newValue) {
                    setState(() {
                      _category = newValue; // here I pop to avoid multiple Dialogs
                    });
                  },
                  items: <String>[
                    "Food",
                    "Entertainment",
                    "Shopping",
                    "Grecories",
                    "Miscellaneous"
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                )
              ],
            )),
        SizedBox(
          height: 10,
        ),
        // Container(
        //     margin: EdgeInsets.symmetric(horizontal: 50),
        //     decoration: BoxDecoration(
        //         border: Border.all(color: Colors.grey, width: 2),
        //         color: Colors.transparent,
        //         borderRadius: BorderRadius.all(Radius.circular(10))),
        //     child: Column( mainAxisAlignment: MainAxisAlignment.center,
        //       children: <Widget>[
        //       Text("Select Amount", style: TextStyle(color: Colors.grey[500]),),
        //       RangeSlider(
        //         values: RangeValues(_starValue, _endValue),
        //         min: 0,
        //         max: 100000,
        //         onChanged: (values) {
        //           setState(() {
        //             _starValue = values.start.roundToDouble();
        //             _endValue = values.end.roundToDouble();
        //           });
        //         },
        //       ),
        //       TextField(
        //         decoration: InputDecoration(
        //         hintText: 'Enter starting amount : ${_starValue.toInt().toString()}',
        //         hintStyle: TextStyle(color: Colors.green),
        //       ),
        //         onChanged: (values) => {
        //           setState((){
        //             _starValue = double.parse(values);
        //           })
        //         },
        //       ),
        //       TextField(
        //         decoration: InputDecoration(
        //         hintText: 'Enter last amount : ${_endValue.toInt().toString()}',
        //         hintStyle: TextStyle(color: Colors.green)
        //       ),
        //       onChanged: (values) => {
        //           setState((){
        //             _endValue = double.parse(values);
        //           })
        //         },
        //       ),
        //     ])),
        SizedBox(
          height: 10,
        ),
         Container(
            margin: EdgeInsets.symmetric(horizontal: 50),
            padding: EdgeInsets.symmetric(horizontal: 100),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 2),
                color: Colors.transparent,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(

              children: <Widget>[
                Text(
                  "Date",
                  style: TextStyle(color: Colors.grey[500]),
                ),
                MaterialButton(
                    padding: EdgeInsets.all(10),
                    color: Colors.grey[500],
                    onPressed: () async {
                      final List<DateTime> picked = await DateRagePicker.showDatePicker(
                          context: context,
                          initialFirstDate: (DateTime.now()).add(new Duration(days: -7)),
                          initialLastDate: DateTime.now(),
                          firstDate: new DateTime(2016),
                          lastDate: new DateTime(2021)
                      );
                      if (picked != null && picked.length == 2) {
                          setState(() {
                            _dateRange = picked.map((date) => date.millisecondsSinceEpoch).toList();
                          });
                      }
                    },
                    child: new Text("pick")
                )
              ])),
             SizedBox(
          height: 10,
        ),
        Row( mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: FlatButton(
          child: Text("Close", style: TextStyle(color: Colors.redAccent),),
          onPressed:() => setState(() {
          _seeFilter = !_seeFilter;
        }))
        ),
        FlatButton(
          child: Text("Reset", style: TextStyle(color: Colors.lightGreen)),
          onPressed:() => setState(() {
          _filterValue = null;
          _category = null;
          _dateRange = [];
        })),
        ],)
      ],
    ));
  }

  Widget _bodyTransactions(_ViewModel model) {
    List<Transaction> list = _seeSearch && _searchValue.isEmpty ? [] : model.appState.transactionList
        .where((transaction) =>
            DateTime.fromMillisecondsSinceEpoch(transaction.date).year ==
            _sortValue)
        .where((transaction) => (_filterValue == null && _category == null)  || (transaction.fromTo == _filterValue || _filterValue == null) && (transaction.category == _category || _category == null))
        .where((transaction) => _dateRange.isEmpty || _dateRange[0] <= transaction.date && transaction.date < _dateRange[1] + 86400000
        )
        .where((transaction) => (! _seeSearch) || 
            ((transaction.fromTo != null &&
                transaction.fromTo.contains(_searchValue)) ||
            (transaction.description != null &&
                transaction.description.contains(_searchValue))))
        .toList();

    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage('assets/img/background.jpg'),
          fit: BoxFit.cover,
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.3), BlendMode.dstATop),
        )),
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Stack(children: <Widget>[
          DraggableScrollableSheet(
            builder: (context, scrollController) {
              return Container(
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.9),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40))),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 24,
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "All Transactions",
                              style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 20,
                                  color: Colors.grey[500]),
                            ),
                            Container(
                                child: Row(
                              children: <Widget>[
                                DropdownButton<String>(
                                  value: _sortValue.toString(),
                                  icon: Icon(
                                    Icons.sort,
                                    color: Colors.grey,
                                  ),
                                  iconSize: 24,
                                  elevation: 16,
                                  style: TextStyle(
                                      color: Colors.grey[500],
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                  underline: Container(
                                      height: 2, color: Colors.transparent),
                                  onChanged: (newValue) {
                                    setState(() {
                                      _sortValue = int.parse(newValue);
                                    });
                                  },
                                  items: <String>[
                                    '2020',
                                    '2019',
                                    '2018',
                                    '2017',
                                    '2016'
                                  ].map<DropdownMenuItem<String>>(
                                      (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                )
                              ],
                            )),
                          ],
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 32),
                      ),
                    
                      Container(
                          margin: EdgeInsets.symmetric(horizontal: 32),
                          child: Row(children: <Widget>[
                            Text("Search",
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold)),
                            IconButton(
                                icon: Icon(Icons.search),
                                color: Colors.grey,
                                onPressed: () => setState(() {
                                      _seeSearch = !_seeSearch;
                                    })),
                            Container(
                                child: Row(children: <Widget>[
                              Text("Filter",
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold)),
                              IconButton(
                                  icon: Icon(Icons.local_bar),
                                  color: Colors.grey,
                                  onPressed: () => setState(() {
                                      _seeFilter = !_seeFilter;
                                    }))
                            ])),
                          ])),
                      Visibility(
                        visible: _seeSearch,
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 32),
                          child: Form(
                              key: _formkey,
                              autovalidate: false,
                              child: Column(children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                                  child: TextFormField(
                                    validator: RequiredValidator(
                                        errorText: 'Required'),
                                    onChanged: (text) => setState(() {
                                      _searchValue = text;
                                    }),
                                    style: TextStyle(
                                        color: Colors.black87, fontSize: 18),
                                    decoration: InputDecoration(
                                      //prefixIcon: Icon(Icons.close ),
                                      suffix: IconButton(
                                        icon: Icon(Icons.cancel, color: Colors.grey,),
                                        onPressed:  (){
                                          setState(() {
                                            _seeSearch = false;
                                          });
                                        },
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.grey)),
                                      labelText: 'Search transaction',
                                      labelStyle: TextStyle(
                                          fontSize: 15, color: Colors.grey),
                                    ),
                                  
                                  ),
                                ),
                              ]
                              )
                              ),
                        ),
                      ),
                      Visibility(
                        visible: _seeFilter,
                        child: _selectFilter(model),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      list.isEmpty
                          ? Center(
                            child: Text(model.appState.transactionList.isEmpty
                              ? "There are no transactions yet"
                              : _seeSearch
                                ? _searchValue.isEmpty 
                                  ? 'Please type the keyword' 
                                  : list.isEmpty 
                                    ? "No matching transactions, please use another keyword"
                                    : ''
                                : list.isEmpty
                                  ? _seeFilter
                                    ? "No matching transactions for the filter, please change the filter"
                                    : "There are no transaction"
                                  : '',
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14,
                                  color: Colors.grey[900],
                                ))
                          )
                          : _buildTransactionsList(list, context, model),
                    ],
                  ),
                  controller: scrollController,
                ),
              );
            },
            initialChildSize: 0.95,
            minChildSize: 0.95,
            maxChildSize: 0.95,
          ),
        ]));
  }

  // Widget _alertDialog(_ViewModel model) => AlertDialog(
  //       title: Text(
  //         "Filter",
  //         textAlign: TextAlign.center,
  //       ),
  //       titleTextStyle: TextStyle(
  //           color: Colors.white, fontSize: 30, fontWeight: FontWeight.w700),
  //       titlePadding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
  //       // content: _selectFilter(model),
  //       contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
  //       contentTextStyle: TextStyle(color: Colors.grey[500], fontSize: 16),
  //       actions: <Widget>[
  //         Container(
  //           height: 40,
  //           width: 100,
  //           child: FlatButton(
  //             child: Text(
  //               "Submit",
  //               style: TextStyle(color: Colors.black),
  //             ),
  //             color: Colors.grey[500],
  //             onPressed: () {
  //               Navigator.pop(context);
  //             },
  //           ),
  //         ),
  //       ],
  //       shape: RoundedRectangleBorder(
  //           side: BorderSide(
  //             style: BorderStyle.none,
  //           ),
  //           borderRadius: BorderRadius.circular(10)),
  //     );
}

class _ViewModel {
  final AppState appState;
  final Function(String) onloadTransaction;

  _ViewModel({this.appState, this.onloadTransaction});

  factory _ViewModel.create(Store<AppState> store) {
    _onloadTransaction(String transaction) {
      store.dispatch(loadTransaction());
    }

    return _ViewModel(
        appState: store.state,
        onloadTransaction: _onloadTransaction);
  }
}
