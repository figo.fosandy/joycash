import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/actions/set_pin.dart';
import 'package:redux/redux.dart';

class SetPinPage extends StatefulWidget {

  @override
  PinState createState() => PinState();
  
}

class PinState extends State<SetPinPage> {
  final GlobalKey<FormState> keyform = GlobalKey<FormState> ();
  String pin;
  String retypePin;
  bool autoValidate = false;

  @override
  Widget build(BuildContext context){
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) => _bodyPin(viewModel),
      );
  }

  Widget _bodyPin(_ViewModel model) {
    return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/img/background.jpg'),
        fit: BoxFit.cover,
      )),
      child: Scaffold(
        backgroundColor: Colors.black87,
        body: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.transparent),
          padding: EdgeInsets.all(45),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 120,
              ),
              Form(
                key: keyform,
                autovalidate: autoValidate,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: TextFormField(
                        validator: pinValidator,
                        obscureText: true,
                        onChanged: (text) => pin = text,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'PIN',
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: TextFormField(
                        onChanged: (text) => retypePin = text,
                        obscureText: true,
                        style: TextStyle(color: Colors.white),
                        validator: (val) => MatchValidator(errorText: 'pin do not match').validateMatch(val, pin),
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'RETYPE YOUR PIN',
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                        child: MaterialButton(
                          onPressed: () => validateInputs(model),
                          child: Text(
                            'SUBMIT',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                          ),
                          color: Colors.lightGreen,
                          elevation: 0,
                          minWidth: 340,
                          height: 60,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
                        ),
                    )
                  ]
                )
              ),
              _tryToSetPin(model.appState)
            ]
          )
            
        )
      )
    );
  }

  Widget _tryToSetPin(AppState appState) {
    if (appState.detailUser.email != null && appState.detailUser.firstName != 'firstname is empty' && appState.detailUser.pin != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, '/login');
      });
    }
    return Text(appState.detailUser.email != null && appState.detailUser.firstName != 'firstname is empty' && appState.detailUser.pin != null ? 'Set pin succesfull' : appState.userLoading ? 'Loading...' : '',
      textAlign: TextAlign.center,
      style: TextStyle(color:  Colors.green)
    );
  }

  final pinValidator = MultiValidator([  
    RequiredValidator(errorText: 'pin is required'),
    MinLengthValidator(4, errorText: 'pin must be 4 characters'),
    MaxLengthValidator(4, errorText: 'pin must be 4 characters'),
    PatternValidator(r'^[a-zA-Z0-9]+$', errorText: 'pin must be alphanumeric'),
    PatternValidator(r'[a-zA-Z]', errorText: 'pin must have at least 1 alphabet'),
    PatternValidator(r'[0-9]', errorText: 'pin must have at least 1 number')
 ]); 

  void validateInputs(_ViewModel model) async{
      if (keyform.currentState.validate()) {
        model.onEditPin(pin);
      } else {
        setState(() {
          autoValidate = true;
        });
      }
  }
}
  
  
class _ViewModel{
  final AppState appState;
  final Function(String) onEditPin;

  _ViewModel({
    this.appState,
    this.onEditPin
  });

  factory _ViewModel.create(Store<AppState> store) {
    _onEditPin(String pin){
      store.dispatch(editPin(pin));
    }

    return _ViewModel(
      appState: store.state,
      onEditPin: _onEditPin
    );
  }
}