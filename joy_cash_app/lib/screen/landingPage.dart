import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/actions/load_amount.dart';
import 'package:joy_cash_app/redux/actions/load_payees.dart';
import 'package:joy_cash_app/redux/actions/loading_request.dart';
import 'package:joy_cash_app/redux/actions/loading_transaction.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:redux/redux.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  void _onLanding(viewModel) {
    final _ViewModel model = viewModel;
    final AppState appState = model.appState;
    String route;
    if (appState.isLogin) {
      route = '/dashboard';
    } else if(appState.detailUser.email != null && appState.detailUser.firstName == 'firstname is empty') {
      route = '/profile';
    } else if(appState.detailUser.email != null && appState.detailUser.firstName != 'firstname is empty' && appState.detailUser.pin == null) {
      route = '/pin';
    } else {
      route = '/login';
    }
    Future.delayed(const Duration(seconds: 3), () {
      if (route == '/dashboard') {
        model.onGetAmount();
        model.onGetPayees();
        model.onLoadRequests();
        model.onLoadTransactions();
      }
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, route);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, _ViewModel>(
        converter: (Store<AppState> store) => _ViewModel.create(store),
        builder: (BuildContext context, _ViewModel viewModel) => Background(),
        onInitialBuild: (_ViewModel viewModel) => _onLanding(viewModel),
      )
    );
  }
}

class Background extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.black12,
      body: Column(
        children: <Widget>[
          new Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Image.asset('assets/img/logo_joycash.png', width: MediaQuery.of(context).size.width/2.5,),
              WavyHeader(),
            ],
          ),
          Expanded(
            child: Container(
            ),
          ),
          Stack(
            alignment: Alignment.bottomLeft,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 200),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('Please wait, the application is preparing'),
                      Loading(indicator: BallPulseIndicator(), size: 100.0)
                    ]
                  ),
                )
              ),
              WavyFooter(),
              CirclePink(),
              CircleYellow(),
            ],
          )
        ],
      ),
    );
  }
}

const List<Color> orangeGradients = [
  Color(0xFFFF9844),
  Color(0xFFFE8853),
  Color(0xFFFD7267),
];

const List<Color> aquaGradients = [
  Color(0xFF5AEAF1),
  Color(0xFF8EF7DA),
];

class WavyHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: TopWaveClipper(),
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: orangeGradients,
              begin: Alignment.topLeft,
              end: Alignment.center),
        ),
        height: MediaQuery.of(context).size.height / 2.5,
      ),
    );
  }
}

class WavyFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: FooterWaveClipper(),
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: aquaGradients,
              begin: Alignment.center,
              end: Alignment.bottomRight),
        ),
        height: MediaQuery.of(context).size.height / 3,
      ),
    );
  }
}

class CirclePink extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Transform.translate(
      offset: Offset(-70.0, 90.0),
      child: Material(
        color: Colors.pink,
        child: Padding(padding: EdgeInsets.all(120)),
        shape: CircleBorder(side: BorderSide(color: Colors.white, width: 15.0)),
      ),
    );
  }
}

class CircleYellow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Transform.translate(
      offset: Offset(0.0, 210.0),
      child: Material(
        color: Colors.yellow,
        child: Padding(padding: EdgeInsets.all(140)),
        shape: CircleBorder(side: BorderSide(color: Colors.white, width: 15.0)),
      ),
    );
  }
}

class TopWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, size.height);

    var firstControlPoint = new Offset(size.width / 7, size.height - 30);
    var firstEndPoint = new Offset(size.width / 6, size.height / 1.5);

    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondControlPoint = Offset(size.width / 5, size.height / 4);
    var secondEndPoint = Offset(size.width / 1.5, size.height / 5);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);

    var thirdControlPoint =
        Offset(size.width - (size.width / 9), size.height / 6);
    var thirdEndPoint = Offset(size.width, 0.0);
    path.quadraticBezierTo(thirdControlPoint.dx, thirdControlPoint.dy,
        thirdEndPoint.dx, thirdEndPoint.dy);

    path.lineTo(size.width, 0.0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class FooterWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.moveTo(size.width, 0.0);
    path.lineTo(size.width, size.height);
    path.lineTo(0.0, size.height);
    path.lineTo(0.0, size.height - 60);
    var secondControlPoint = Offset(size.width - (size.width / 6), size.height);
    var secondEndPoint = Offset(size.width, 0.0);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class YellowCircleClipper extends CustomClipper<Rect> {
  @override
  Rect getClip(Size size) {
    return null;
  }

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) => false;
}

class _ViewModel {
  final AppState appState;
  final Function() onLoadTransactions;
  final Function() onLoadRequests;
  final Function() onGetAmount;
  final Function() onGetPayees;

  _ViewModel({
    this.appState,
    this.onLoadTransactions,
    this.onLoadRequests,
    this.onGetAmount,
    this.onGetPayees
  });

  factory _ViewModel.create(Store<AppState> store) {
    _onLoadTransactions() {
      store.dispatch(loadTransaction());
    }

    _onLoadRequest() {
      store.dispatch(loadRequest());
    }

    _onGetAmount() {
      store.dispatch(getAmount());
    }
    
    _onGetPayees() {
      store.dispatch(getPayees());
    }

    return _ViewModel(
      appState: store.state,
      onLoadTransactions: _onLoadTransactions,
      onLoadRequests: _onLoadRequest,
      onGetAmount: _onGetAmount,
      onGetPayees: _onGetPayees
    );
  }
}