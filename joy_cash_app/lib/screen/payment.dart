import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/request.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/redux/actions/loading_request.dart';
import 'package:joy_cash_app/redux/actions/loading_transaction.dart';
import 'package:joy_cash_app/redux/actions/payment.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:redux/redux.dart';

class PaymentPage extends StatefulWidget {
  PaymentPage({Key key}) : super(key: key);
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<PaymentPage> {
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  String _payee;
  String _category;
  int _amount = 1000;
  String _description;
  String _pin;
  bool _setPin = false;

  List<DropdownMenuItem<String>> added() => [
    DropdownMenuItem<String>(
      child: Text('Add new payee'),
      value: '',
    )
  ];

  void _onDispose(Store<AppState> store) {
    if (store.state.requestId.isNotEmpty) {
      store.dispatch(loadRequest());
    }
    store.dispatch(loadTransaction());
    store.dispatch(ResetStatus());
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) =>
          _bodyPayment(viewModel),
      onDispose: (Store<AppState> store) => _onDispose(store),
    );
  }

  Widget _bodyPayment(_ViewModel model) {
    Request _request = model.appState.requestId.isEmpty ? null : model.appState.requestList.firstWhere((req) => req.id == model.appState.requestId);
    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage('assets/img/background.jpg'),
          fit: BoxFit.cover,
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.3), BlendMode.dstATop),
        )),
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: DraggableScrollableSheet(
          expand: true,
          builder: (context, scrollController) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.9),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40))),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 24,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Payment Transactions",
                            style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 24,
                                color: Colors.black),
                          ),
                          Text(
                            "...",
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                                color: Colors.grey[800]),
                          )
                        ],
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 32),
                    ),
                    SizedBox(height: 24),
                    Visibility(
                      visible: model.appState.status.contains('payment'),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            model.appState.status == 'payment' 
                              ? Loading(indicator: BallPulseIndicator(), size: 100.0, color: Colors.green)
                              : Icon(model.appState.status.contains('Success') ? Icons.check : Icons.close, 
                                  color: model.appState.status.contains('Success') ? Colors.green : Colors.redAccent,
                                  size: 100.0
                              ),
                            Text(model.appState.status.contains('Success') ? 'Payment successful' : model.appState.status.contains('Failed') ? model.appState.paymentErrorMessages : 'Initiating payment...',
                              style: TextStyle(color: model.appState.status.contains('Failed') ? Colors.redAccent : Colors.blue),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        )
                      )
                    ),
                    Visibility(
                      visible: ! model.appState.status.contains('payment'),
                      child: Column(
                        children: <Widget>[
                          Form(
                            key: formkey,
                            autovalidate: true,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.all(20),
                                    child: DropdownButtonFormField(
                                        validator: (_) {
                                          if (_request != null)
                                            return null;
                                          if (_payee == null || _payee.isEmpty)
                                            return 'Please chose payee';
                                          return null;
                                        },
                                        onChanged: _request != null ? null : (text) => {
                                          if (text.isEmpty) {
                                            Navigator.pushNamed(context, '/payee')
                                          },
                                          setState(() {
                                            _payee = text.isEmpty ? null : text;
                                          })
                                        },
                                        disabledHint: Text(_request != null ? _request.from : '',
                                          style: TextStyle(color: Colors.blue)
                                        ),
                                        style: TextStyle(color: Colors.blue),
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.only(left: 0),
                                          filled: _request!= null || _payee != null,
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide:
                                                  BorderSide(color: Colors.blue)),
                                          hintStyle: TextStyle(
                                              fontSize: 15, color: Colors.blue),
                                          labelText: 'Payee',
                                          labelStyle: TextStyle(
                                              fontSize: 15, color: Colors.blue),
                                        ),
                                        value: _request != null ? _request.from : _payee,
                                        isDense: true,
                                        items: <String>[
                                              ...model.appState.detailUser.payees
                                            ].map<DropdownMenuItem<String>>(
                                                (String value) {
                                              return DropdownMenuItem<String>(
                                                child: Text(value),
                                                value: value,
                                              );
                                            }).toList() +
                                            added())),
                                Padding(
                                  padding: EdgeInsets.all(20),
                                  child: TextFormField(
                                    enabled: _request == null,
                                    initialValue: (_request != null ? _request.amount : _amount).toString(),
                                    validator: MultiValidator([
                                      RequiredValidator(
                                        errorText: 'Please fill the amount'
                                      ),
                                      RangeValidator(
                                          min: 1000,
                                          max: 100000000,
                                          errorText: 'invalid amount'),
                                    ]),
                                    keyboardType: TextInputType.number,
                                    onChanged: (text) => setState(() {
                                      _amount = int.parse(text);
                                    }),
                                    style: TextStyle(color: Colors.blue),
                                    decoration: InputDecoration(
                                        disabledBorder: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.blue)),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.blue)),
                                        labelText: 'Amount',
                                        labelStyle: TextStyle(
                                            fontSize: 15, color: Colors.blue)),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(20),
                                  child: TextFormField(
                                    initialValue: _request != null ? _request.description : _description,
                                    validator: RequiredValidator(
                                        errorText: 'Please fill the description'),
                                    keyboardType: TextInputType.text,
                                    maxLines: null,
                                    maxLength: 200,
                                    onChanged: (text) => setState(() {
                                      _description = text;
                                    }),
                                    style: TextStyle(color: Colors.blue),
                                    decoration: InputDecoration(
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.blue)),
                                        labelText: 'Decription',
                                        labelStyle: TextStyle(
                                            fontSize: 15, color: Colors.blue)),
                                  ),
                                ),
                                Padding(
                                    padding: EdgeInsets.all(20),
                                    child: DropdownButtonFormField(
                                        validator: (_) {
                                          if (_category == null || _category.isEmpty)
                                            return 'Please chose category';
                                          return null;
                                        },
                                        onChanged: (text) => setState(() {
                                              _category = text;
                                            }),
                                        style: TextStyle(color: Colors.blue),
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.only(left: 0),
                                          filled: _category != null,
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide:
                                                  BorderSide(color: Colors.blue)),
                                          hintStyle: TextStyle(
                                              fontSize: 15, color: Colors.blue),
                                          labelText: 'Category',
                                          labelStyle: TextStyle(
                                              fontSize: 15, color: Colors.blue),
                                        ),
                                        value: _category,
                                        isDense: true,
                                        items: <String>[
                                          'Food',
                                          'Entertainment',
                                          'Shopping',
                                          'Grecories',
                                          'Miscellaneous'
                                        ].map<DropdownMenuItem<String>>(
                                            (String value) {
                                          return DropdownMenuItem<String>(
                                            child: Text(value),
                                            value: value,
                                          );
                                        }).toList()))
                              ],
                            ),
                          ),
                          Visibility(
                            visible: ! _setPin,
                            child: Container(
                              padding:
                                  EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                              margin: EdgeInsets.symmetric(horizontal: 32),
                              alignment: Alignment.center,
                              child: Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: MaterialButton(
                                  onPressed: formkey.currentState == null || ! formkey.currentState.validate() || (_request != null && _request.amount > model.appState.detailUser.amount) ? null : () => {
                                    setState(() {
                                      _setPin = true;
                                    }),
                                  },
                                  child: Text(
                                    (_request != null && _request.amount > model.appState.detailUser.amount) ? 'Insuficient funds' : 'Click to process',
                                    style: TextStyle(
                                        color: formkey.currentState == null || ! formkey.currentState.validate() || (_request != null && _request.amount > model.appState.detailUser.amount) ? Colors.lightGreen : Colors.white,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  color: Colors.lightGreen,
                                  elevation: 0,
                                  minWidth: 350,
                                  height: 60,
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                        color: Colors.lightGreen,
                                        width: 2.0,
                                        style: BorderStyle.solid
                                      ),
                                      borderRadius: BorderRadius.circular(50)),
                                ),
                              ),
                            ),
                          ),
                          Visibility(
                              visible: _setPin,
                              child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 2),
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 50, vertical: 50),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.blue),
                                      color: Colors.transparent,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(10))),
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        'INPUT PIN',
                                        style: TextStyle(color: Colors.blue),
                                      ),
                                      TextFormField(
                                        decoration: InputDecoration(
                                          labelText: 'PIN',
                                          labelStyle: TextStyle(color: Colors.blue),
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide:
                                                  BorderSide(color: Colors.blue)),
                                        ),
                                        style: TextStyle(color: Colors.blue),
                                        obscureText: true,
                                        maxLength: 4,
                                        onChanged: (text) => setState(() {
                                          _pin = text;
                                        }),
                                      ),
                                      Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            MaterialButton(
                                              onPressed: () {
                                                if (_request == null) {
                                                  model.onPayment(_pin, _amount, _payee, _description, _category);
                                                } else {
                                                  model.onPayment(_pin, _request.amount, _request.from, _description == null ? _request.description : _description, _category);
                                                }
                                              },
                                              child: Text(
                                                'Submit',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.bold),
                                              ),
                                              color: Colors.lightGreen,
                                              elevation: 0,
                                              minWidth: 100,
                                              height: 60,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(50)),
                                            ),
                                            MaterialButton(
                                              onPressed: () => setState(() {_setPin = false;}),
                                              child: Text(
                                                'Cancel',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.bold),
                                              ),
                                              color: Colors.redAccent,
                                              elevation: 0,
                                              minWidth: 100,
                                              height: 60,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(50)),
                                            ),
                                          ]),
                                    ],
                                  )))
                        ],
                      ),
                    ),
                  ],
                ),
                controller: scrollController,
              ),
            );
          },
          initialChildSize: 0.95,
          maxChildSize: 0.95,
        ));
  }
}

class _ViewModel {
  final AppState appState;
  final Function(String, int, String, String, String) onPayment;

  _ViewModel({
    this.appState,
    this.onPayment
  });

  factory _ViewModel.create(Store<AppState> store) {
    _onPayment(String pin, int amount, String to, String description, String category) {
      store.dispatch(payment(pin, amount, to, description, category));
    }

    return _ViewModel(
      appState: store.state,
      onPayment: _onPayment
    );
  }
}