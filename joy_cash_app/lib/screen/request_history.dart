import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/request.dart';
import 'package:joy_cash_app/redux/actions/loading_request.dart';
import 'package:redux/redux.dart';

class RequestHistoryPage extends StatefulWidget {
  RequestHistoryPage({Key key}) : super(key: key);
  @override
  RequestHistoryPageState createState() => RequestHistoryPageState();
}

class RequestHistoryPageState extends State<RequestHistoryPage> {
  String _sortValue = 'pending';

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) =>
          _bodyRequests(viewModel),
    );
  }

  ListView _buildRequestsList(
      List<Request> list, context, _ViewModel model) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: list.length,
        padding: EdgeInsets.all(0),
        controller: ScrollController(keepScrollOffset: false),
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 5),
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey[300],
                      blurRadius: 10.0,
                      spreadRadius: 4.5)
                ]),
            child: Row(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.all(Radius.circular(18))),
                  child: Icon(
                    list[index].status == 'pending' ? Icons.access_time : Icons.check,
                    color: Colors.lightBlue[900],
                  ),
                  padding: EdgeInsets.all(12),
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        list[index].status ?? '',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[900]),
                      ),
                      Text(
                        list[index].from == model.appState.detailUser.email 
                          ? model.appState.userList.firstWhere((user) => user.email == list[index].to).uniqueId
                          : model.appState.userList.firstWhere((user) => user.email == list[index].from).uniqueId,
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[500]),
                      ),
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      FlutterMoneyFormatter(
                        amount: list[index].amount == null ? 0 : list[index].amount.abs().toDouble(),
                        settings: MoneyFormatterSettings(
                          symbol: 'IDR'
                        ),
                      ).output.compactSymbolOnLeft,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: list[index].status == 'pending' ? Colors.redAccent : Colors.lightGreen),
                    ),
                  Text(DateTime.fromMillisecondsSinceEpoch(list[index].date).toString().substring(0, 10), style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700, color: Colors.grey[500]),),
                  ],
                ),
              ],
            ),
          );
        });
  }

  Widget _bodyRequests(_ViewModel model) {
    List<Request> list = model.appState.requestList
                            .where((request) => request.from == model.appState.detailUser.email)
                            .where((request) => _sortValue == 'all' || request.status == _sortValue)
                            .toList();
    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage('assets/img/background.jpg'),
          fit: BoxFit.cover,
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.3), BlendMode.dstATop),
        )),
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Stack(children: <Widget>[
          DraggableScrollableSheet(
            builder: (context, scrollController) {
              return Container(
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.9),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40))),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 24,
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Request from me",
                              style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 24,
                                  color: Colors.grey[500]),
                            ),
                            Container(
                              child: 
                            DropdownButton<String>(
                              value: _sortValue,
                              icon: Icon(Icons.sort, color: Colors.grey,),
                              iconSize: 24,
                              elevation: 16,
                              hint: Text('Year', style: TextStyle(color: Colors.grey[500]),),
                              style: TextStyle(color: Colors.grey[500], fontSize: 20, fontWeight: FontWeight.bold),
                              underline: Container(
                                height: 2,
                                color: Colors.grey,
                              ),
                              onChanged: (newValue) {
                                setState(() {
                                  _sortValue = newValue;
                                });
                              },
                              items: <String>[
                                'all',
                                'paid',
                                'pending',
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            )
                            ),
                          ],
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 32),
                      ),
                      SizedBox(
                        height: 24,
                      ),

                      list.isEmpty
                          ? Padding(
                            padding: EdgeInsets.all(10),
                            child: Center(
                              child: Text("There are no ${_sortValue == 'paid' ? 'paid ' : ''}requests${_sortValue == 'all' ? ' yet' : ''}",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14,
                                  color: Colors.grey[900],
                                )
                              )
                            )
                          )
                          : _buildRequestsList(list, context, model)
                    ],
                  ),
                  controller: scrollController,
                ),
              );
            },
            initialChildSize: 0.95,
            minChildSize: 0.95,
            maxChildSize: 0.95,
          ),
        ]));
  }
}

class _ViewModel {
  final AppState appState;
  final Function(String) onloadRequest;

  _ViewModel({this.appState, this.onloadRequest});

  factory _ViewModel.create(Store<AppState> store) {
    _onloadRequest(String transaction) {
      store.dispatch(loadRequest());
    }

    return _ViewModel(
        appState: store.state, onloadRequest: _onloadRequest);
  }
}
