import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:redux/redux.dart';
import 'package:joy_cash_app/model/transaction.dart';
import 'package:joy_cash_app/redux/actions/loading_transaction.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) =>
          _bodyDashboard(viewModel),
    );
  }

  Future _showDialog(_ViewModel model) async {
    return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Are you sure ?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Yes'),
              onPressed: () {
                Navigator.pop(context);
                model.onLogout();
                Navigator.of(context).pushReplacementNamed('/login');
              }
            ),
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.pop(context);
              }
            )
          ]
        );
      }
    );
  }

  ListView _buildTransactionList(
      List<Transaction> list, context, _ViewModel model) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: list.length,
      padding: EdgeInsets.all(0),
      controller: ScrollController(keepScrollOffset: false),
      itemBuilder: (context, index) {
        return GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/detailTransaction',
                  arguments: list[index]);
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 32, vertical: 5),
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey[300],
                        blurRadius: 10.0,
                        spreadRadius: 4.5)
                  ]),
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color: list[index].amount < 0
                            ? Colors.redAccent
                            : Colors.lightGreen,
                        borderRadius: BorderRadius.all(Radius.circular(18))),
                    child: Icon(list[index].amount < 0
                        ? Icons.call_made
                        : list[index].type == "topup"
                            ? Icons.loupe
                            : Icons.call_received),
                    padding: EdgeInsets.all(12),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          list[index].type.toUpperCase(),
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              color: Colors.grey[900]),
                        ),
                        Text(
                          list[index].fromTo != null ? model.appState.userList.firstWhere((user) => user.email == list[index].fromTo).uniqueId : '',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w700,
                              color: Colors.grey[500]),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        FlutterMoneyFormatter(
                          amount: list[index].amount == null ? 0 : list[index].amount.abs().toDouble(),
                          settings: MoneyFormatterSettings(
                            symbol: '${list[index].amount.isNegative ? '-' : '+'} IDR'
                          ),
                        ).output.compactSymbolOnLeft,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            color: list[index].amount < 0
                                ? Colors.redAccent
                                : Colors.lightGreen),
                      ),
                      Text(
                        DateTime.fromMillisecondsSinceEpoch(list[index].date)
                            .toString()
                            .substring(0, 10),
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[500]),
                      ),
                    ],
                  ),
                ],
              ),
            ));
      },
    );
  }

  Widget _bodyDashboard(_ViewModel model) {
    String balance = FlutterMoneyFormatter(
      amount: model.appState.detailUser.amount == null ? 0 : model.appState.detailUser.amount.toDouble(),
      settings: MoneyFormatterSettings(
        symbol: 'IDR'
      ),
    ).output.compactSymbolOnLeft;

    List<Transaction> list = model.appState.transactionList.take(5).toList();
    int responseItem = model.appState.requestList
        .where((request) =>
            request.to == model.appState.detailUser.email &&
            request.status == 'pending')
        .length;

    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/img/background.jpg'),
        fit: BoxFit.cover,
        colorFilter: new ColorFilter.mode(
            Colors.black.withOpacity(0.3), BlendMode.dstATop),
      )),
      height: MediaQuery.of(context).size.height,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 32, vertical: 32),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Hi, ${model.appState.detailUser.firstName}!',
                      style: TextStyle(
                          color: Colors.blue,
                          fontSize: 30,
                          fontWeight: FontWeight.bold),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Stack(children: <Widget>[
                            IconButton(
                              icon: Icon(
                                Icons.library_books,
                                color: Colors.lightBlue[100],
                              ),
                              onPressed: () {
                                Navigator.of(context)
                                    .pushNamed('/responseHistory');
                              },
                              padding: EdgeInsets.all(1.0),
                            ),
                            responseItem == 0
                                ? Container()
                                : Positioned(
                                    right: 5,
                                    top: 5,
                                    child: CircleAvatar(
                                        radius: 8,
                                        backgroundColor: Colors.red,
                                        child: Text(responseItem.toString(),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 8,
                                                fontWeight: FontWeight.bold))))
                          ]),
                          SizedBox(
                            width: 16,
                          ),
                          CircleAvatar(
                            radius: 25,
                            backgroundColor: Colors.white,
                            child: ClipOval(
                              child: PopupMenuButton(
                                offset: Offset(0, 100),
                                icon: Icon(Icons.person_pin, size: 50, color: Colors.blue),
                                padding: EdgeInsets.all(0),
                                onSelected: (text) {
                                  if (text == 'logout') {
                                    _showDialog(model);
                                  }
                                },
                                itemBuilder: (context) => [
                                  PopupMenuItem(
                                    child: Row(
                                      children: <Widget>[
                                        Icon(Icons.power_settings_new, color: Colors.white),
                                        Text('Logout', style: TextStyle(color: Colors.white))
                                      ],
                                    ),
                                    value: 'logout',
                                  )
                                ]
                              )
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                Text(
                  balance,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                      fontWeight: FontWeight.w700),
                ),
                Text(
                  "Available Balance",
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 16,
                      color: Colors.blue[100]),
                ),
                SizedBox(
                  height: 24,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.9),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(18))),
                            child: IconButton(
                              icon: Icon(Icons.send),
                              color: Colors.blue[900],
                              iconSize: 50,
                              onPressed: () {
                                Navigator.of(context).pushNamed('/payment');
                              },
                            ),
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          Text(
                            "Send",
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 14,
                                color: Colors.blue[100]),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.9),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(18))),
                            child: IconButton(
                              icon: Icon(Icons.call_missed_outgoing),
                              color: Colors.blue[900],
                              iconSize: 50,
                              onPressed: () {
                                Navigator.of(context)
                                    .pushNamed('/requestHistory');
                              },
                            ),
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          Text(
                            "Request",
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 14,
                                color: Colors.blue[100]),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.9),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(18))),
                            child: IconButton(
                                icon: Icon(Icons.attach_money),
                                color: Colors.blue[900],
                                iconSize: 50,
                                onPressed: () =>
                                    Navigator.pushNamed(context, '/topup')),
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          Text(
                            "Topup",
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 14,
                                color: Colors.blue[100]),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.9),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(18))),
                            child: IconButton(
                                icon: Icon(Icons.book),
                                color: Colors.blue[900],
                                iconSize: 50,
                                onPressed: () =>
                                    Navigator.of(context).pushNamed('/payee')),
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          Text(
                            "Payee",
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 14,
                                color: Colors.blue[100]),
                          ),
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),

          DraggableScrollableSheet(
            builder: (context, scrollController) {
              return Container(
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.9),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40))),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 24,
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Recent Transactions",
                              style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 24,
                                  color: Colors.black),
                            ),
                            GestureDetector(
                                onTap: () {
                                  Navigator.of(context)
                                      .pushNamed('/transactionHistory');
                                },
                                child: Text(
                                  "See all",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16,
                                      color: Colors.grey[800]),
                                )),
                          ],
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 32),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      list.isEmpty
                          ? Center(
                            child: Text("There are no transactions yet",
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 14,
                                color: Colors.grey[900],
                              ))
                          )
                          : _buildTransactionList(list, context, model)
                    ],
                  ),
                  controller: scrollController,
                ),
              );
            },
            initialChildSize: 0.65,
            minChildSize: 0.65,
            maxChildSize: 1,
          )
        ],
      ),
    );
  }
}

class _ViewModel {
  final AppState appState;
  final Function(String) onloadTransaction;
  final Function() onLogout;

  _ViewModel({this.appState, this.onloadTransaction, this.onLogout});

  factory _ViewModel.create(Store<AppState> store) {
    _onloadTransaction(String transaction) {
      store.dispatch(loadTransaction());
    }

    _onLogout() {
      store.dispatch(Logout());
    }

    return _ViewModel(
        appState: store.state, onloadTransaction: _onloadTransaction, onLogout: _onLogout);
  }
}
