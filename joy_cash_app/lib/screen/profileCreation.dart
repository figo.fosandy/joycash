import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/user.dart';
import 'package:joy_cash_app/redux/actions/edit_profile.dart';
import 'package:redux/redux.dart';


class ProfilePage extends StatefulWidget {
  ProfilePage({Key key}) : super(key: key);
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<ProfilePage> {
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  bool autoValidate = false;
  String firstname;
  String lastname;
  String uniqueId;
  String address;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) => _bodyProfile(viewModel),
    );
  }

  Widget _bodyProfile(_ViewModel model) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/img/background.jpg'),
        fit: BoxFit.cover,
      )),
      child: Scaffold(
        backgroundColor: Colors.black87,
        body: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.transparent),
          padding: EdgeInsets.all(45),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 120,
              ),
              Form(
                key: formkey,
                autovalidate: autoValidate,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: TextFormField(
                        validator: validateName,
                        onChanged: (text) => firstname = text,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'Firstname',
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: TextFormField(
                        validator: validateName,
                        onChanged: (text) => lastname = text,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'Lastname',
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: TextFormField(
                        validator: (text) => validateUniqueId(text, model.appState.userList),
                        style: TextStyle(color: Colors.white),
                        onChanged: (text) => uniqueId = text,
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'Unique Id',
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: TextFormField(
                        validator: validateAddress,
                        onChanged: (text) => address = text,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'Address',
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
                child: MaterialButton(
                  onPressed: () => validateInputs(model),
                  child: Text(
                    'SAVE PROFILE',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                  color: Colors.lightGreen,
                  elevation: 0,
                  minWidth: 350,
                  height: 60,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)),
                ),
              ),
              _tryToCreateProfile(model.appState)
            ],
          ),
        ),
      ),
    );
  }

  String validateName(String value) {
    RegExp regex = new RegExp(
        r"^[a-zA-Z]{0,12}$");
    if (!regex.hasMatch(value))
      return 'cannot contain special character and number';
    else if (value == "")
      return 'firstname or lastname cant be empty';
    else
      return null;
  }

  String validateUniqueId(String value, List<User> list) {
    if (value != uniqueId || value == "")
      return 'unique id cant be empty';
    if (list.any((user) => user.uniqueId == value))
      return 'unique id has been used';
    else
      return null;
  }

  Widget _tryToCreateProfile(AppState appState) {
    if (appState.detailUser.email != null && appState.detailUser.firstName != 'firstname is empty') {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, '/pin');
      });
    }
    return Text(appState.detailUser.email != null && appState.detailUser.firstName != 'firstname is empty' ? 'Create Profile succesfull' : appState.userLoading ? 'Loading...' : '',
      textAlign: TextAlign.center,
      style: TextStyle(color:  Colors.green)
    );
  }

  String validateAddress(String value) {

    if (value == "")
      return 'please add an address';
    else
      return null;
  }

  void validateInputs(_ViewModel model) async{
    if (formkey.currentState.validate()) {
      Map data = {
        'firstName': firstname,
        'lastName': lastname,
        'address': address,
        'uniqueId': uniqueId
      };
      model.onCreateProfile(data);
      formkey.currentState.save();
    } else {
      setState(() {
        autoValidate = true;
      });
    }
  }

}

class _ViewModel {
  final AppState appState;
  final Function(Map) onCreateProfile;

  _ViewModel({
    this.appState,
    this.onCreateProfile
  });

  factory _ViewModel.create(Store<AppState> store) {
    _onCreateProfile(Map data) {
      store.dispatch(editProfile(data));
    }

    return _ViewModel(
      appState: store.state,
      onCreateProfile: _onCreateProfile
    );
  }
}