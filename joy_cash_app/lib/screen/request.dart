import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/actions/create_request.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/redux/actions/loading_request.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:redux/redux.dart';

class RequestPage extends StatefulWidget {
  RequestPage({Key key}) : super(key: key);
  @override
  _RequestState createState() => _RequestState();
}

class _RequestState extends State<RequestPage> {
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  String _payee;
  int _amount;
  String _description;

  List<DropdownMenuItem<String>> added() => [
    DropdownMenuItem<String>(
      child: Text('Add new payee'),
      value: '',
    )
  ];

  void _onDispose(Store<AppState> store) {
    store.dispatch(loadRequest());
    store.dispatch(ResetStatus());
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) =>
          _bodyRequest(viewModel),
      onDispose: (Store<AppState> store) => _onDispose(store),
    );
  }

  Widget _bodyRequest(_ViewModel model) {
    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage('assets/img/background.jpg'),
          fit: BoxFit.cover,
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.3), BlendMode.dstATop),
        )),
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: DraggableScrollableSheet(
          expand: true,
          builder: (context, scrollController) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.9),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40))),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 24,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Request Transactions",
                            style: TextStyle(
                                fontWeight: FontWeight.w900,
                                fontSize: 24,
                                color: Colors.black),
                          ),
                          Text(
                            "...",
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                                color: Colors.grey[800]),
                          )
                        ],
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 32),
                    ),
                    SizedBox(height: 24),
                    Visibility(
                      visible: model.appState.status.contains('request'),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            model.appState.status == 'request' 
                              ? Loading(indicator: BallPulseIndicator(), size: 100.0, color: Colors.green)
                              : Icon(model.appState.status.contains('Success') ? Icons.check : Icons.close, 
                                  color: model.appState.status.contains('Success') ? Colors.green : Colors.redAccent,
                                  size: 100.0
                              ),
                            Text(model.appState.status.contains('Success') ? 'Request successful' : model.appState.status.contains('Failed') ? model.appState.requestErrorMessages : 'Initiating Request...',
                              style: TextStyle(color: model.appState.status.contains('Failed') ? Colors.redAccent : Colors.blue),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        )
                      )
                    ),
                    Visibility(
                      visible: ! model.appState.status.contains('request'),
                      child: Column(
                        children: <Widget>[
                          Form(
                            key: formkey,
                            autovalidate: true,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.all(20),
                                    child: DropdownButtonFormField(
                                        validator: (_) {
                                          if (_payee == null || _payee.isEmpty)
                                            return 'Please chose payee';
                                          return null;
                                        },
                                        onChanged: (text) => {
                                          if (text.isEmpty) {
                                            Navigator.pushNamed(context, '/payee')
                                          },
                                          setState(() {
                                            _payee = text.isEmpty ? null : text;
                                          })
                                        },
                                        style: TextStyle(color: Colors.blue),
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.only(left: 0),
                                          filled: _payee != null,
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide:
                                                  BorderSide(color: Colors.blue)),
                                          hintStyle: TextStyle(
                                              fontSize: 15, color: Colors.blue),
                                          labelText: 'Payee',
                                          labelStyle: TextStyle(
                                              fontSize: 15, color: Colors.blue),
                                        ),
                                        value: _payee,
                                        isDense: true,
                                        items: <String>[
                                              ...model.appState.detailUser.payees
                                            ].map<DropdownMenuItem<String>>(
                                                (String value) {
                                              return DropdownMenuItem<String>(
                                                child: Text(value),
                                                value: value,
                                              );
                                            }).toList() +
                                            added())),
                                Padding(
                                  padding: EdgeInsets.all(20),
                                  child: TextFormField(
                                    validator: MultiValidator([
                                      RequiredValidator(
                                        errorText: 'Please fill the amount'
                                      ),
                                      RangeValidator(
                                          min: 1000,
                                          max: 100000000,
                                          errorText: 'invalid amount'),
                                    ]),
                                    keyboardType: TextInputType.number,
                                    onChanged: (text) => setState(() {
                                      _amount = int.parse(text);
                                    }),
                                    style: TextStyle(color: Colors.blue),
                                    decoration: InputDecoration(
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.blue)),
                                        labelText: 'Amount',
                                        labelStyle: TextStyle(
                                            fontSize: 15, color: Colors.blue)),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(20),
                                  child: TextFormField(
                                    validator: RequiredValidator(
                                        errorText: 'Please fill the description'),
                                    keyboardType: TextInputType.text,
                                    maxLines: null,
                                    maxLength: 200,
                                    onChanged: (text) => setState(() {
                                      _description = text;
                                    }),
                                    style: TextStyle(color: Colors.blue),
                                    decoration: InputDecoration(
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.blue)),
                                        labelText: 'Decription',
                                        labelStyle: TextStyle(
                                            fontSize: 15, color: Colors.blue)),
                                  ),
                                ),
                              ],
                            ),
                          ),
                            Container(
                              padding:
                                  EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                              margin: EdgeInsets.symmetric(horizontal: 32),
                              alignment: Alignment.center,
                              child: Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: MaterialButton(
                                  onPressed: formkey.currentState == null || ! formkey.currentState.validate() ? null : () => {
                                      model.onRequest(_amount, _payee, _description)
                                  },
                                  child: Text(
                                    'Click to process',
                                    style: TextStyle(
                                        color: formkey.currentState == null || ! formkey.currentState.validate() ? Colors.lightGreen : Colors.white,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  color: Colors.lightGreen,
                                  elevation: 0,
                                  minWidth: 350,
                                  height: 60,
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                        color: Colors.lightGreen,
                                        width: 2.0,
                                        style: BorderStyle.solid
                                      ),
                                      borderRadius: BorderRadius.circular(50)),
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
                controller: scrollController,
              ),
            );
          },
          initialChildSize: 0.95,
          maxChildSize: 0.95,
        ));
  }
}

class _ViewModel {
  final AppState appState;
  final Function(int, String, String) onRequest;

  _ViewModel({
    this.appState,
    this.onRequest
  });

  factory _ViewModel.create(Store<AppState> store) {
    _onRequest(int amount, String to, String description) {
      store.dispatch(request(amount, to, description));
    }

    return _ViewModel(
      appState: store.state,
      onRequest: _onRequest
    );
  }
}
