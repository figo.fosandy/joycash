import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/actions/login.dart';
import 'package:redux/redux.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _email;
  String _password;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) => _bodyLogin(viewModel),
    );
  }

  Widget _bodyLogin(_ViewModel model) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/img/background.jpg'),
          fit: BoxFit.cover,
      )),
      child: Scaffold(
        backgroundColor: Colors.black87,
        body: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.transparent),
          padding: EdgeInsets.all(45),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 120,
              ),
              Form(
                key: _formkey,
                autovalidate: _autoValidate,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: TextFormField(
                        validator: RequiredValidator(errorText: 'Required'),
                        onChanged: (text) => _email = text,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'Email',
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: TextFormField(
                        validator: RequiredValidator(errorText: 'Required'),
                        obscureText: true,
                        onChanged: (text) => _password = md5.convert(utf8.encode(text)).toString(),
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'Password',
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
                child: MaterialButton(
                  onPressed: () => validateInputs(model),
                  child: Text(
                    'LOG IN',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                  color: Colors.lightGreen,
                  elevation: 0,
                  minWidth: 350,
                  height: 60,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)),
                ),
              ),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Don't have an account? "),
                  GestureDetector(
                    onTap: () {
                        Navigator.pushReplacementNamed(context, '/register');
                    },
                    child: Text('Register',
                      style: TextStyle(color: Colors.greenAccent, fontWeight: FontWeight.bold)
                    )
                  )
                ],
              ),
              _tryToLogin(model.appState)
            ],
          ),
        ),
      ),
    );
  }

  Widget _tryToLogin(AppState appState) {
    if (appState.isLogin) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, '/dashboard');
      });
    }
    return Text(appState.isLogin ? 'Login succesfull' : appState.userLoading ? 'Loading...' : appState.failLogin ?? '',
      textAlign: TextAlign.center,
      style: TextStyle(color: appState.failLogin == null || appState.userLoading ? Colors.green : appState.failLogin.isEmpty ? Colors.green : Colors.red)
    );
  }

  void validateInputs(_ViewModel model) {
    if (_formkey.currentState.validate()) {
      _formkey.currentState.save();
      model.onLogin(_email, _password);
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }
}

class _ViewModel {
  final AppState appState;
  final Function(String, String) onLogin;

  _ViewModel({
    this.appState,
    this.onLogin
  });

  factory _ViewModel.create(Store<AppState> store) {
    _onLogin(String username, String password) {
      store.dispatch(login(username, password));
    }

    return _ViewModel(
      appState: store.state,
      onLogin: _onLogin
    );
  }
}