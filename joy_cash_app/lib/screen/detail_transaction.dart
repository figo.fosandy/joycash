import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/transaction.dart';
import 'package:joy_cash_app/redux/actions/loading_transaction.dart';
import 'package:redux/redux.dart';

class DetailTransactionPage extends StatefulWidget {
  final Transaction _transaction;
  DetailTransactionPage(this._transaction, {arguments});
  @override
 _DetailTransactionState createState() => _DetailTransactionState (_transaction);

}

class _DetailTransactionState extends State<DetailTransactionPage>{
    final Transaction _transaction;
    _DetailTransactionState(this._transaction);
 
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) => _bodyDetailTransaction(viewModel, _transaction),
      );
  }


  Widget _bodyDetailTransaction(_ViewModel model, Transaction _detail) {
    
    return Container(
    decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/img/background.jpg'),
          fit: BoxFit.cover,
          colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
      )),
      height: MediaQuery.of(context).size.height,
      width: double.infinity,
      child: DraggableScrollableSheet(
              expand: true,
              builder: (context, scrollController){
                return Container(
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.9),
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(40), topRight: Radius.circular(40))
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 24,),
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("Detail Transaction", style: TextStyle(fontWeight: FontWeight.w900, fontSize: 24, color: Colors.black),),
                            ],
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 32),
                        ),
                        
                         SizedBox(height: 16,),
                           Card(
                             color: Colors.white,
                             child: Column(
                               children: <Widget>[
                                 ListTile(
                                   title: Text('Date Transaction', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700, color: Colors.grey[500])),
                                   subtitle: Text(DateTime.fromMillisecondsSinceEpoch(_transaction.date).toString().substring(0, 10), style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.black87),)
                                 )
                               ],
                             ),
                           ),
                           Card(
                             color: Colors.white,
                             child: Column(
                               children: <Widget>[
                                 ListTile(
                                   title: Text('Type',style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700, color: Colors.grey[500])),
                                   subtitle:  Text(_transaction.type.toUpperCase(), style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.black87),),
                                 )
                               ],
                             ),
                           ),
                           Visibility(
                             visible: _transaction.category != null,
                             child: Card(
                                color: Colors.white,
                                child: Column(
                                  children: <Widget>[
                                    ListTile(
                                      title: Text('Category',style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700, color: Colors.grey[500])),
                                      subtitle:  Text(_transaction.category ?? '', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.black87),),
                                    )
                                  ],
                                ),
                           ),
                           ),
                           
                           Visibility(
                             visible: _transaction.description != null ,
                             child: Card(
                              color: Colors.white,
                              child: Column(
                                children: <Widget>[
                                  ListTile(
                                    title: Text('Description',style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700, color: Colors.grey[500])),
                                    subtitle:  Text(_transaction.description ?? '', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.black87),),
                                  )
                                ],
                              ),
                            ),
                           ),
                           
                           Card(
                             color: Colors.white,
                             child: Column(
                               children: <Widget>[
                                 ListTile(
                                   title: Text('Amount',style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700, color: Colors.grey[500])),
                                   subtitle: Text(
                                     FlutterMoneyFormatter(
                                        amount: _transaction.amount == null ? 0 : _transaction.amount.abs().toDouble(),
                                        settings: MoneyFormatterSettings(
                                          symbol: '${_transaction.amount.isNegative ? '-' : '+'} IDR',
                                          fractionDigits: 0
                                        ),
                                      ).output.symbolOnLeft,
                                     style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: _transaction.amount < 0 ? Colors.redAccent : Colors.lightGreen),),
                                 )
                               ],
                             ),
                           ),
                           Visibility(visible: _transaction.fromTo != null, 
                           child:Card(
                             color: Colors.white,
                             child: Column(
                               children: <Widget>[
                                 ListTile(
                                   title: Text('Payee',style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700, color: Colors.grey[500])),
                                   subtitle:  Text(_transaction.fromTo ?? '', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.black87),),
                                 )
                               ],
                             ),
                           ))
                           

                      ],
                    ),
                   controller: scrollController,
                  ),
                );
                
              },
              initialChildSize: 0.95,
              maxChildSize: 0.95,
              )
              );
  
  }
              
            
    
  }
  


 
class _ViewModel{
  final AppState appState;
  final Function() onloadTransaction;

  _ViewModel({
    this.appState,
    this.onloadTransaction
  });

  factory _ViewModel.create(Store<AppState> store) {
    _onloadTransaction(){
      store.dispatch(loadTransaction());
    }

    return _ViewModel(
      appState: store.state,
      onloadTransaction: _onloadTransaction
    );
  }
}

