import 'package:flutter/material.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/user.dart';
import 'package:joy_cash_app/redux/actions/register.dart';
import 'dart:convert';

import 'package:redux/redux.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key}) : super(key: key);
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<RegisterPage> {
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  bool autoValidate = false;
  String email;
  String password;
  String retypePassword;
  String address;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) => _bodyRegister(viewModel),
    );
  }

  Widget _bodyRegister(_ViewModel model) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/img/background.jpg'),
        fit: BoxFit.cover,
      )),
      child: Scaffold(
        backgroundColor: Colors.black87,
        body: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.transparent),
          padding: EdgeInsets.all(45),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 120,
              ),
              Form(
                key: formkey,
                autovalidate: autoValidate,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: TextFormField(
                        validator: (text) => validateEmail(text, model.appState.userList),
                        onChanged: (text) => email = text,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'Email',
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: TextFormField(
                        validator: validatePassword,
                        obscureText: true,
                        onChanged: (text) => password = md5.convert(utf8.encode(text)).toString(),
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'Password',
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: TextFormField(
                        validator: validateRetype,
                        obscureText: true,
                        onChanged: (text) => retypePassword = md5.convert(utf8.encode(text)).toString(),
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            labelText: 'Retype Password',
                            labelStyle:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
                child: MaterialButton(
                  onPressed: () => validateInputs(model),
                  child: Text(
                    'SIGN UP',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                  color: Colors.lightGreen,
                  elevation: 0,
                  minWidth: 350,
                  height: 60,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)),
                ),
              ),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Already have an account? ",
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),
                  GestureDetector(
                    onTap: () {
                        Navigator.pushReplacementNamed(context, '/login');
                    },
                    child: Text('Login',
                      style: TextStyle(color: Colors.greenAccent, fontWeight: FontWeight.bold)
                    )
                  )
                ],
              ),
              _tryToRegister(model.appState)
            ],
          ),
        ),
      ),
    );
  }

  String validateEmail(String value, List<User> list) {
    if (value == null || value.isEmpty)
      return 'Email needed';
    if (! value.contains('@'))
      return 'Invalid email';
    if (list.any((user) => user.email == value))
      return 'Email has been used';
    else
      return null;
  }

  String validatePassword(String value) {
    if (value.length < 8)
      return 'Password must be more than 8 character';
    else
      return null;
  }

  String validateRetype(String value) {
    if (retypePassword != password || value == "")
      return 'retype password not match and cant be empty';
    else
      return null;
  }

  Widget _tryToRegister(AppState appState) {
    if (appState.detailUser.email != null && appState.detailUser.firstName == 'firstname is empty') {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, '/profile');
      });
    }
    return Text(appState.detailUser.email != null && appState.detailUser.firstName == 'firstname is empty' ? 'Register succesfull' : appState.userLoading ? 'Loading...' : '',
      textAlign: TextAlign.center,
      style: TextStyle(color:  Colors.green)
    );
  }

  void validateInputs(_ViewModel model) async{
    if (formkey.currentState.validate()) {
      model.onRegister(email, password);
    } else {
      setState(() {
        autoValidate = true;
      });
    }
  }

}

class _ViewModel {
  final AppState appState;
  final Function(String, String) onRegister;

  _ViewModel({
    this.appState,
    this.onRegister
  });

  factory _ViewModel.create(Store<AppState> store) {
    _onRegister(String email, String password) {
      store.dispatch(register(email, password));
    }

    return _ViewModel(
      appState: store.state,
      onRegister: _onRegister
    );
  }
}