import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/user.dart';
import 'package:joy_cash_app/redux/actions/add_payee.dart';
import 'package:redux/redux.dart';

class AddPayeePage extends StatefulWidget {
  AddPayeePage({Key key}) : super(key: key);
  @override
  _AddPayeeState createState() => _AddPayeeState();
}
class _AddPayeeState extends State<AddPayeePage> {
    String _search = '';

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) => _bodyaddPayee(viewModel),
    );
  }



  ListView _buildUserList(List<User> list, context, _ViewModel model) {
   
    return ListView.builder(
      shrinkWrap: true,
      itemCount: list.length,
      itemBuilder: (context, int) {
        bool _notAdded = model.appState.detailUser.payees.isEmpty ? true
                      : ! model.appState.detailUser.payees.any((payee) => payee == list[int].email);
        return new Card(
          color: Colors.blue,
          child:Column(
            children: <Widget>[
               ListTile( 
                              title: Text(list[int].uniqueId, style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14, color: Colors.grey[900])),
                              subtitle: Text(list[int].email, style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14, color: Colors.grey[900])),
                              leading: new CircleAvatar(
                                  backgroundColor: Colors.grey,
                                  child: Text(list[int].email.substring(0, 1))
                                  ),
                              trailing:
                               IconButton(
                                icon: _notAdded ? Icon(Icons.add)  :  Icon(Icons.check, color: Colors.teal,) ,
                                onPressed: 
                                  _notAdded ? ()  {
                                    model.onaddPayee(list[int].email);
                                    _message(context);
                                   } : null
               
                              ),
                             
                )
              ],
            )
          );
        }
    );
  }

  void _message (BuildContext context){
    final message = SnackBar(
      content: Text('You have added Payee to your list'),
      backgroundColor: Colors.transparent,
      action: SnackBarAction(
        label: 'Ok',
        textColor: Colors.purple,
        onPressed: () {
        },
      ),
    );
    Scaffold.of(context).showSnackBar(message);
  }

  Widget _bodyaddPayee(_ViewModel model) {

    List<User> list = _search.isEmpty ? [] : model.appState.userList
      .where((user) => user.email != model.appState.detailUser.email && ! user.firstName.contains('empty'))
      .where((user) => user.email.contains(_search) || user.uniqueId.contains(_search))
      .toList();
    
    return Container(
    decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/img/background.jpg'),
          fit: BoxFit.cover,
          colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
      )),
      height: MediaQuery.of(context).size.height,
      width: double.infinity,
      child: DraggableScrollableSheet(
              expand: true,
              builder: (context, scrollController){
                return Container(
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.9),
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(40), topRight: Radius.circular(40))
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 24,),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("Search Payee", style: TextStyle(fontWeight: FontWeight.w900, fontSize: 24, color: Colors.black),),
                              Text("...", style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16, color: Colors.grey[800]),)
                            ],
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 32),
                        ),
                        SizedBox(height: 24,),
                            Container(
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextField(
                                      style: TextStyle(fontSize: 20, color: Colors.black45),
                                      onChanged: (value) {
                                        setState(() {
                                          _search = value;
                                        });
                                      },
                                      decoration: InputDecoration(
                                        
                                          labelText: "Search",
                                          labelStyle: TextStyle(color: Colors.blue),
                                          prefixIcon: Icon(Icons.search, color: Colors.blue),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(25.0)),
                                            borderSide: BorderSide(
                                              color: Colors.blue
                                            )
                                          ),
                                          focusedBorder:  OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(25.0)),
                                            borderSide: BorderSide(
                                              color: Colors.blue
                                            )
                                          ),
                                        ),
                                    ),    
                                  ),                                    
                                  list.isEmpty ? Text(model.appState.userList.isEmpty
                                  ? 'there are no user' 
                                  : _search.isEmpty ? 'please type the keyword'
                                  : list.isEmpty
                                   ? 'ID is not registered on JoyCash'
                                  :'', 
                                   style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14, color: Colors.grey[900],))
                                  : _buildUserList(list, context, model)
                                     

                                  
                      ],
                    ),
                  ),
                 ]
                    )
                  )
                );
              },
              initialChildSize: 0.95,
              maxChildSize: 0.95,
            )
    );
  }
  
}


  class _ViewModel {
    final AppState appState;
            final Function(String) onaddPayee;
          
            _ViewModel({
              this.appState,
              this.onaddPayee
            });
          
            factory _ViewModel.create(Store<AppState> store) {
              _onaddPayee(String payee){
                store.dispatch(addPayee(payee));
              }
          
              return _ViewModel(
                appState: store.state,
                onaddPayee: _onaddPayee
              );
            }
          }
