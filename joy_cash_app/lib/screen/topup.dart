import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/redux/actions/loading_transaction.dart';
import 'package:joy_cash_app/redux/actions/topup.dart';
import 'package:joy_cash_app/redux/actions/topup_verify.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:redux/redux.dart';

class TopupPage extends StatefulWidget {
  TopupPage({Key key}) : super(key: key);
  @override
  _TopupState createState() => _TopupState();
}

class _TopupState extends State<TopupPage> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  bool autoValidate = true;
  int _topupAmount = 1000;
  List<bool> isSelected;
  bool _showpin = false;
  bool _showVerify = false;
  bool _hiddenAll = false;
  String type;
  String pin;

  void _onDispose(Store<AppState> store) {
    store.dispatch(loadTransaction());
    store.dispatch(ResetStatus());
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (Store<AppState> store) => _ViewModel.create(store),
      builder: (BuildContext context, _ViewModel viewModel) =>
          _bodyTopup(viewModel),
      onDispose: (Store<AppState> store) => _onDispose(store),
    );
  }

  @override
  void initState() {
    isSelected = [true, false];
    super.initState();
  }

  Widget _bodyTopup(_ViewModel model) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: double.infinity,
      child: DraggableScrollableSheet(
        builder: (context, scrollController) {
          return Container(
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 24,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "TOPUP METHOD",
                              style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 24,
                                  color: Colors.black),
                            ),
                            Text(
                              "1 Bank Account , and 1 Credit Card",
                              style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  fontSize: 14,
                                  color: Colors.grey),
                            ),
                          ],
                        ),
                        IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.more_horiz,
                            color: Colors.lightBlue[900],
                            size: 30,
                          ),
                        )
                      ],
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 32),
                  ),

                  SizedBox(
                    height: 16,
                  ),

                  Container(
                    alignment: Alignment.center,
                    child: Row(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 32),
                          child: Row(
                            children: <Widget>[
                              ToggleButtons(
                                selectedBorderColor: Colors.white,
                                selectedColor: Colors.blue,
                                fillColor: Colors.blue,
                                borderRadius: BorderRadius.circular(50),
                                children: <Widget>[
                                  Container(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    child: Text(
                                      "Bank Account",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14,
                                          color: Colors.grey[900]),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                  ),
                                  Container(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    child: Text(
                                      "Credit Card",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14,
                                          color: Colors.grey[900]),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 10),
                                  ),
                                ],
                                onPressed: (int index) {
                                  setState(() {
                                    for (int i = 0;
                                        i < isSelected.length;
                                        i++) {
                                      if (i == index) {
                                        isSelected[i] = true;
                                        setState(() {
                                          type = 'Credit Card';
                                        });
                                      } else {
                                        isSelected[i] = false;
                                        setState(() {
                                          type = 'Bank Account';
                                        });
                                      }
                                    }
                                  });
                                },
                                isSelected: isSelected,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),

                  SizedBox(
                    height: 16,
                  ),

                  Container(
                      margin: EdgeInsets.symmetric(horizontal: 32),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          image: new DecorationImage(
                            image: new AssetImage('assets/img/background.jpg'),
                            fit: BoxFit.cover,
                            colorFilter: new ColorFilter.mode(
                                Colors.black.withOpacity(0.6),
                                BlendMode.dstATop),
                          ),
                          color: Colors.grey),
                      padding: EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              CircleAvatar(
                                radius: 16,
                                backgroundColor:
                                    Color.fromRGBO(50, 172, 121, 1),
                                child: Icon(
                                  Icons.check,
                                  color: Colors.white,
                                  size: 24,
                                ),
                              ),
                              Text(
                                '${type ?? 'Bank Account'} ',
                                style: TextStyle(
                                    fontStyle: FontStyle.normal,
                                    fontSize: 22,
                                    color: Colors.pinkAccent,
                                    fontWeight: FontWeight.w900),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 32,
                          ),
                          SizedBox(
                            height: 32,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Account",
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.black87,
                                        fontWeight: FontWeight.w700,
                                        letterSpacing: 2.0),
                                  ),
                                  Text(
                                    model.appState.detailUser.uniqueId ?? '',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black87,
                                        fontWeight: FontWeight.w700,
                                        letterSpacing: 2.0),
                                  ),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "your pin",
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.black87,
                                        fontWeight: FontWeight.w700,
                                        letterSpacing: 2.0),
                                  ),
                                  Text(
                                    "${model.appState.detailUser.pin.replaceRange(0, 2, "**")}" ?? '',
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black87,
                                        fontWeight: FontWeight.w700,
                                        letterSpacing: 2.0),
                                  ),
                                ],
                              )
                            ],
                          )
                        ],
                      )),

                  SizedBox(
                    height: 16,
                  ),

                  Form(
                    key: _formkey,
                    autovalidate: autoValidate,
                    child: Container(
                    decoration: BoxDecoration(
                        color: !_hiddenAll ? Colors.grey[500] : Colors.transparent,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey[100],
                              spreadRadius: 10.0,
                              blurRadius: 4.5)
                        ]),
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                    margin: EdgeInsets.symmetric(horizontal: 32, vertical: 12),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Visibility(
                              visible: !_hiddenAll,
                              child: Column(
                                children: <Widget>[
                                  Icon(
                                    Icons.account_balance_wallet,
                                    color: Colors.limeAccent,
                                    size: 50,
                                  ),
                                  SizedBox(
                                    width: 16,
                                  ),
                                  Text(
                                    "How much do you want to top up?",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18,
                                        color: Colors.white),
                                  ),
                                  Container(
                                      decoration: BoxDecoration(
                                        color: Colors.grey[500],
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 12),
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 32),
                                      child: TextFormField(
                                        validator: topupValidator,
                                        keyboardType:
                                            TextInputType.numberWithOptions(
                                                signed: true),
                                        onChanged: (text) =>
                                        setState((){
                                            _topupAmount = int.parse(text);
                                        }),
                                        style: TextStyle(color: Colors.white),
                                        decoration: InputDecoration(
                                            prefixText: "Rp. ",
                                            hintText: '1000',
                                            hintStyle:
                                                TextStyle(color: Colors.white),
                                            enabledBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.white)),
                                            labelText: "Amount",
                                            labelStyle: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white)),
                                      )),
                                  Visibility(
                                    visible: !_showpin,
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 4),
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 32),
                                      alignment: Alignment.center,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: 20),
                                        child: MaterialButton(
                                          onPressed: _formkey.currentState == null || !_formkey.currentState.validate() ? null : () => {
                                            setState(() {
                                              _showpin = true;
                                            }),
                                            model.onTopup(_topupAmount),
                                          },
                                          child: Text(
                                            'Click to Process',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          color: Colors.lightGreen,
                                          elevation: 0,
                                          minWidth: 350,
                                          height: 60,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50)),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  ),
                  
                  SizedBox(
                    height: 16,
                  ),

                  Visibility(
                      visible: _showpin,
                      child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                          margin: EdgeInsets.symmetric(
                              horizontal: 32, vertical: 10),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              border: Border.all(color:model.appState.transactionLoading ? Colors.grey :  model.appState.topupErrorMessages == "" ? Colors.green : Colors.redAccent, width: 8),
                              color: Colors.grey[500],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                          BoxShadow(
                              color: model.appState.transactionLoading ? Colors.grey :  model.appState.topupErrorMessages == "" ? Colors.green : Colors.redAccent,
                              spreadRadius: 3.0,
                              blurRadius: 4),
                        ]),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                    model.appState.transactionLoading ? "loading.. please wait" 
                                    :  model.appState.topupErrorMessages == ""
                                    ? 'OTP SENT TO YOUR EMAIL, please fill for confirmation' 
                                    : model.appState.topupErrorMessages ?? '',
                                style: TextStyle(color: Colors.white),
                              ),
                              Container(
                                  decoration: BoxDecoration(
                                    color: Colors.grey[500],
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 4),
                                  margin: EdgeInsets.symmetric(horizontal: 32),
                                  child: TextFormField(
                                    enabled: model.appState.status == "topupSuccess",
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            signed: true),
                                    obscureText: true,
                                    onChanged: (text) => pin = text,
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                        hintStyle:
                                            TextStyle(color: Colors.white),
                                        enabledBorder: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.white)),
                                        labelText: "OTP VERIFICATION",
                                        labelStyle: TextStyle(
                                            fontSize: 15, color: Colors.white)),
                                  )),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    MaterialButton(
                                      onPressed: model.appState.status == "topupSuccess" ? () => {
                                        model.onVerifyTopup(pin, _topupAmount),
                                        setState(() {
                                          _showpin = false;
                                          _showVerify = true;
                                          _hiddenAll = true;
                                        })
                                      } : null,
                                      child: Text(
                                        'Submit',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      color: Colors.lightGreen,
                                      elevation: 0,
                                      minWidth: 100,
                                      height: 30,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(50)),
                                    ),
                                    MaterialButton(
                                      onPressed: () => setState(() {
                                        _showpin = !_showpin;
                                      }),
                                      child: Text(
                                        'Cancel',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      color: Colors.redAccent,
                                      elevation: 0,
                                      minWidth: 100,
                                      height: 30,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(50)),
                                    ),
                                  ]),
                            ],
                          ))),
                  _tryVerifyTopup(model.appState)
                ],
              ),
            ),
            decoration: BoxDecoration(
                color: Color.fromRGBO(243, 245, 248, 1),
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(40),
                    topLeft: Radius.circular(40))),
          );
        },
        initialChildSize: 0.95,
        maxChildSize: 0.95,
      ),
    );
  }

  Widget _tryVerifyTopup(AppState appState) {
    if (appState.status == "verifySuccess") {
      return Visibility(
          visible: _showVerify,
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 50),
              margin: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.lightGreen, width: 5),
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(children: <Widget>[
                Icon(Icons.check_circle, color: Colors.lightGreen, size: 50),
                Text('Top up succesfull',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.green, fontSize: 20))
              ])));
    } else if (appState.status == "verify") {
      return Visibility(
          visible: _showVerify,
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 50),
              margin: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.transparent,
              ),
              child:  Loading(indicator: BallPulseIndicator(), size: 100.0, color: Colors.blue)));
    } else {
      return Visibility(
          visible: _showVerify,
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 50),
              margin: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.redAccent, width: 5),
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(children: <Widget>[
                Icon(Icons.error, color: Colors.redAccent, size: 50),
                Text(appState.verifyErrorMessages ?? '',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.redAccent, fontSize: 20))
            ])));
    }
  }

final topupValidator = MultiValidator([  
    RequiredValidator(errorText: 'Top up cannot be empty'),
    RangeValidator(min: 1000, max: 100000000, errorText: "min 1000 and max 100.000.000"),
]); 
}

class _ViewModel {
  final AppState appState;
  final Function(int) onTopup;
  final Function(String, int) onVerifyTopup;

  _ViewModel({this.appState, this.onTopup, this.onVerifyTopup});

  factory _ViewModel.create(Store<AppState> store) {
    _onTopup(int amount) {
      store.dispatch(topUp(amount));
    }

    _onVerifyTopup(String pin, int amount) {
      store.dispatch(topUpVerify(pin, amount));
    }

    return _ViewModel(
        appState: store.state,
        onTopup: _onTopup,
        onVerifyTopup: _onVerifyTopup);
  }
}
