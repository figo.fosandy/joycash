import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/transaction.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class TransactionSucces{
  final List<Transaction> list;

  TransactionSucces(this.list);
}

ThunkAction<AppState> loadTransaction(){
  return (Store<AppState> store) async {
    store.dispatch(TransactionLoading('loadTransaction'));
    String uri = await loadAsset();
    Dio dio = Dio();
    await dio.get('$uri/user/transaction/${store.state.detailUser.email}')
      .then((res) => {
        store.dispatch(TransactionSucces(
          List.from(res.data).map((transaction) => Transaction.fromJson(transaction)).toList()
        ))
      });
  };
}


