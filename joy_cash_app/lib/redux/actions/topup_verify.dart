import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/actions/load_amount.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class TopupVerifySuccess {}

class TopupVerifyFailed {
  final String message;

  TopupVerifyFailed(this.message);
}

ThunkAction<AppState> topUpVerify(String pin, int amount) {
  return (Store<AppState> store) async {
    store.dispatch(TransactionLoading('verify'));
    String uri = await loadAsset();
    Dio dio = Dio();
    Map data = {
      'pin' : pin,
      'amount': amount
    };
    await dio.post('$uri/user/verifytopup/${store.state.detailUser.email}/${store.state.detailUser.token}', data: data)
      .then((res) => {
          store.dispatch(getAmount()),
          store.dispatch(TopupVerifySuccess())
      })
      .catchError((err) {
        store.dispatch(TopupVerifyFailed(err.response.data['message']));
      });
  };
}