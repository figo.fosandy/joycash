import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class GetAmount {
  int amount;

  GetAmount(this.amount);
}

ThunkAction<AppState> getAmount() {
  return (Store<AppState> store) async {
    String uri = await loadAsset();
    Dio dio = Dio();
    await dio.get('$uri/user/amount/${store.state.detailUser.email}')
      .then((res) => {
        store.dispatch(GetAmount(res.data['amount']))
      });
  };
}