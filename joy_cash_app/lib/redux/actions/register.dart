import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/user.dart';
import 'package:joy_cash_app/redux/actions/load_user_list.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class RegisterSuccess {
  final User detailUser;

  RegisterSuccess(this.detailUser);
}

ThunkAction<AppState> register(String email, String password) {
  return (Store<AppState> store) async {
    store.dispatch(UserLoading());
    String uri = await loadAsset();
    Dio dio = Dio();
    Map data = {
      'email': email,
      'password': password
    };
    await dio.post('$uri/user/register', data: data)
      .then((res) => {
          store.dispatch(RegisterSuccess(User.fromJson(res.data))),
          store.dispatch(loadUserList())
      });
  };
}