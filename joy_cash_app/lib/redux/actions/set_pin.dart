import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/user.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';



class EditPinSuccess {
  final User detailUser;

  EditPinSuccess(this.detailUser);
}

ThunkAction<AppState> editPin(String pin){
  return (Store<AppState> store) async {
    store.dispatch(UserLoading());
    String uri = await loadAsset();
    Dio dio = Dio();
    Map data = {
      'pin': pin
    };
    await dio.put('$uri/user/pin/${store.state.detailUser.email}', data: data)
      .then((res) => {
        store.dispatch(EditPinSuccess(User.fromJson(res.data)))
      });
  };
}