import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/user.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class LoadingListSuccess {
  final List<User> list;

  LoadingListSuccess(this.list);
}

ThunkAction<AppState> loadUserList() {
  return (Store<AppState> store) async {
    store.dispatch(UserLoading());
    String uri = await loadAsset();
    Dio dio = Dio();
    await dio.get('$uri/user/list')
      .then((res) => {
          store.dispatch(
            LoadingListSuccess(
              List.from(res.data).map((user) => User.fromJson(user)).toList()
            )
          )
      });
  };
}