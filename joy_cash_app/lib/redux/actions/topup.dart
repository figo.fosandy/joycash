import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class TopupSuccess {
  final String token;
  TopupSuccess(this.token);
}

class TopupFailed {
  final String message;
  TopupFailed(this.message);
}

ThunkAction<AppState> topUp(int amount) {
  return (Store<AppState> store) async {
    store.dispatch(TransactionLoading('topup'));
    String uri = await loadAsset();
    Dio dio = Dio();
    Map data = {
      'amount': amount
    };
    await dio.post('$uri/user/topup/${store.state.detailUser.email}', data: data)
      .then((res) => {
          store.dispatch(TopupSuccess(res.data['token'].toString()))
      })
      .catchError((err) {
        store.dispatch(TopupFailed(err.response.data['message']));
      });
  };
}