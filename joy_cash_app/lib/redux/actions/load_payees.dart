import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class GetPayees {
  List<String> payees;

  GetPayees(this.payees);
}

ThunkAction<AppState> getPayees() {
  return (Store<AppState> store) async {
    String uri = await loadAsset();
    Dio dio = Dio();
    await dio.get('$uri/user/payees/${store.state.detailUser.email}')
      .then((res) => {
        store.dispatch(GetPayees(List<String>.from(res.data['payees'])))
      });
  };
}