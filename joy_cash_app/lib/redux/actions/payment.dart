import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/actions/load_amount.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/redux/actions/response.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class PaymentSuccess {}

class PaymentFailed {
  final String message;

  PaymentFailed(this.message);
}

ThunkAction<AppState> payment(String pin, int amount, String to, String description, String category) {
  return (Store<AppState> store) async {
    store.dispatch(TransactionLoading('payment'));
    String uri = await loadAsset();
    Dio dio = Dio();
    Map data = {
      'from': store.state.detailUser.email,
      'to': to,
      'pin' : pin,
      'amount': amount,
      'description': description,
      'category': category
    };
    await dio.post('$uri/user/payment', data: data)
      .then((res) => {
          if(store.state.requestId.isNotEmpty) {
            store.dispatch(response(store.state.requestId))
          } else {
            store.dispatch(getAmount()),
            store.dispatch(PaymentSuccess()),
          }
      })
      .catchError((err) {
        store.dispatch(PaymentFailed(err.response.data['message']));
      });
  };
}