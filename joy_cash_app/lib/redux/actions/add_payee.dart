import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/user.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class AddPayeeSuccess{
  final User detailUser;

  AddPayeeSuccess(this.detailUser);
}

ThunkAction<AppState> addPayee(String payee){
  return (Store<AppState> store) async {
    store.dispatch(UserLoading());
    String uri = await loadAsset();
    Dio dio = Dio();
    Map data = {
      'email': payee
    };
    await dio.put('$uri/user/addPayee/${store.state.detailUser.email}', data: data)
      .then((res) => {
        store.dispatch(AddPayeeSuccess(User.fromJson(res.data)))
      });
  };
}
