import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/actions/load_amount.dart';
import 'package:joy_cash_app/redux/actions/payment.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';


class PayRequest{
  final String data;

  PayRequest(this.data);
}

ThunkAction<AppState> response(String id) {
  return (Store<AppState> store) async {
    String uri = await loadAsset();
    Dio dio = Dio();
    await dio.post('$uri/user/response/$id')
      .then((res) => {
        store.dispatch(getAmount()),
        store.dispatch(PaymentSuccess()),
      });
  };
}