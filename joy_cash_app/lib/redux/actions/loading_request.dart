import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/request.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class LoadRequestSuccess{
  final List<Request> list;

  LoadRequestSuccess(this.list);
}

ThunkAction<AppState> loadRequest(){
  return (Store<AppState> store) async {
    String uri = await loadAsset();
    Dio dio = Dio();
    await dio.get('$uri/user/request_list/${store.state.detailUser.email}')
      .then((res) => {
        store.dispatch(LoadRequestSuccess(
          List.from(res.data).map((request) => Request.fromJson(request)).toList()
        ))
      });
  };
}