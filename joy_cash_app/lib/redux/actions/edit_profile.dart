import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/user.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class EditProfileSuccess {
  final User detailUser;

  EditProfileSuccess(this.detailUser);
}

ThunkAction<AppState> editProfile(Map data) {
  return (Store<AppState> store) async {
    store.dispatch(UserLoading());
    String uri = await loadAsset();
    Dio dio = Dio();
    await dio.put('$uri/user/profile/${store.state.detailUser.email}', data: data)
      .then((res) => {
          store.dispatch(EditProfileSuccess(User.fromJson(res.data)))
      });
  };
}