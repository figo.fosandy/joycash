import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/actions/load_amount.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class RequestSuccess {}

class RequestFailed {
  final String message;

  RequestFailed(this.message);
}

ThunkAction<AppState> request(int amount, String to, String description) {
  return (Store<AppState> store) async {
    store.dispatch(TransactionLoading('request'));
    String uri = await loadAsset();
    Dio dio = Dio();
    Map data = {
      'from': store.state.detailUser.email,
      'to': to,
      'amount': amount,
      'description': description
    };
    await dio.post('$uri/user/request', data: data)
      .then((res) => {
          store.dispatch(getAmount()),
          store.dispatch(RequestSuccess()),
      })
      .catchError((err) {
        store.dispatch(RequestFailed(err.response.data['message']));
      });
  };
}