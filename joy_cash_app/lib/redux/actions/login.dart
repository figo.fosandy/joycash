import 'package:dio/dio.dart';
import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/user.dart';
import 'package:joy_cash_app/redux/actions/load_user_list.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/redux/actions/loading_request.dart';
import 'package:joy_cash_app/redux/actions/loading_transaction.dart';
import 'package:joy_cash_app/src/loadConfig.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class LoginSuccess {
  final User detailUser;

  LoginSuccess(this.detailUser);
}

class LoginFailed {
  final String message;

  LoginFailed(this.message);
}

ThunkAction<AppState> login(String username, String password) {
  return (Store<AppState> store) async {
    store.dispatch(UserLoading());
    String uri = await loadAsset();
    Dio dio = Dio();
    Map data = {
      '${username.contains('@') ? 'email' : 'uniqueId'}' : username ,
      'password': password
    };
    await dio.post('$uri/user/login', data: data)
      .then((res) => {
          store.dispatch(LoginSuccess(User.fromJson(res.data))),
          store.dispatch(loadUserList()),
          store.dispatch(loadTransaction()),
          store.dispatch(loadRequest())
      })
      .catchError((err) {
        store.dispatch(LoginFailed(err.response.data['message']));
      });
  };
}