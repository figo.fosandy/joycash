import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/model/user.dart';
import 'package:joy_cash_app/redux/actions/add_payee.dart';
import 'package:joy_cash_app/redux/actions/create_request.dart';
import 'package:joy_cash_app/redux/actions/edit_profile.dart';
import 'package:joy_cash_app/redux/actions/load_amount.dart';
import 'package:joy_cash_app/redux/actions/load_payees.dart';
import 'package:joy_cash_app/redux/actions/load_user_list.dart';
import 'package:joy_cash_app/redux/actions/loading.dart';
import 'package:joy_cash_app/redux/actions/loading_request.dart';
import 'package:joy_cash_app/redux/actions/loading_transaction.dart';
import 'package:joy_cash_app/redux/actions/login.dart';
import 'package:joy_cash_app/redux/actions/payment.dart';
import 'package:joy_cash_app/redux/actions/register.dart';
import 'package:joy_cash_app/redux/actions/response.dart';
import 'package:joy_cash_app/redux/actions/set_pin.dart';
import 'package:joy_cash_app/redux/actions/topup.dart';
import 'package:joy_cash_app/redux/actions/topup_verify.dart';
import 'package:redux/redux.dart';

AppState appStateReducer(AppState state, action) {
  return userReducer(state, action);
}

Reducer<AppState> userReducer = combineReducers<AppState>([
  TypedReducer<AppState, UserLoading>(loadingReducer),
  TypedReducer<AppState, LoginSuccess>(loginSuccessReducer),
  TypedReducer<AppState, LoginFailed>(loginFailedReducer),
  TypedReducer<AppState, RegisterSuccess>(registerSuccessReducer),
  TypedReducer<AppState, EditPinSuccess>(editPinSuccessReducer),
  TypedReducer<AppState, EditProfileSuccess>(editProfileSuccessReducer),
  TypedReducer<AppState, LoadingListSuccess>(loadingListSuccessReducer),
  TypedReducer<AppState, AddPayeeSuccess>(addPayeeSuccessReducer),
  TypedReducer<AppState, TopupSuccess>(topupSuccessReducer),
  TypedReducer<AppState, TopupFailed>(topupFailReducer),
  TypedReducer<AppState, TransactionLoading>(loadingTransactionReducer),
  TypedReducer<AppState, TopupVerifySuccess>(verifySuccessReducer),
  TypedReducer<AppState, TopupVerifyFailed>(verifyFailReducer),
  TypedReducer<AppState, PaymentSuccess>(paymentSuccessReducer),
  TypedReducer<AppState, PaymentFailed>(paymentFailReducer),
  TypedReducer<AppState, GetAmount>(getAmountReducer),
  TypedReducer<AppState, TransactionSucces>(transactionSuccesReducer),
  TypedReducer<AppState, ResetStatus>(resetStatusReducer),
  TypedReducer<AppState, RequestSuccess>(requestSuccessReducer),
  TypedReducer<AppState, RequestFailed>(requestFailReducer),
  TypedReducer<AppState, LoadRequestSuccess>(loadRequestSuccessReducer),
  TypedReducer<AppState, PayRequest>(payRequestReducer),
  TypedReducer<AppState, Logout>(logoutReducer),
  TypedReducer<AppState, GetPayees>(getPayeesReducer)
]);

AppState loadingReducer(AppState state, UserLoading action) {
  return state.copyWith(userLoading: true);
}

AppState loginSuccessReducer(AppState state, LoginSuccess action) {
  return state.copyWith(userLoading: false, failLogin: '', isLogin: true, detailUser: action.detailUser);
}

AppState loginFailedReducer(AppState state, LoginFailed action) {
  return state.copyWith(userLoading: false, failLogin: action.message, isLogin: false, detailUser: User());
}

AppState registerSuccessReducer(AppState state, RegisterSuccess action) {
  return state.copyWith(userLoading: false, detailUser: action.detailUser);
}

AppState editPinSuccessReducer(AppState state, EditPinSuccess action){
  return state.copyWith(userLoading: false, detailUser: action.detailUser, failLogin: '');
}

AppState editProfileSuccessReducer(AppState state, EditProfileSuccess action) {
  return state.copyWith(userLoading: false, detailUser: action.detailUser, failLogin: '');
}

AppState loadingListSuccessReducer(AppState state, LoadingListSuccess action) {
  return state.copyWith(userLoading: false, userList: action.list);
}

AppState addPayeeSuccessReducer(AppState state, AddPayeeSuccess action){
  return state.copyWith(userLoading: false, detailUser: action.detailUser);
}

AppState topupSuccessReducer(AppState state, TopupSuccess action) {
  return state.copyWith(transactionLoading: false, detailUser: state.detailUser.copyWith(token: action.token), status: 'topupSuccess', topupErrorMessages: '');
}

AppState topupFailReducer(AppState state, TopupFailed action) {
  return state.copyWith(transactionLoading: false, status: 'topupFail', topupErrorMessages: action.message);
}

AppState verifySuccessReducer(AppState state, TopupVerifySuccess action) {
  return state.copyWith(transactionLoading: false, status: 'verifySuccess', verifyErrorMessages: '');
}

AppState verifyFailReducer(AppState state, TopupVerifyFailed action) {
  return state.copyWith(transactionLoading: false, status: 'verifyFailed', verifyErrorMessages: action.message);
}

AppState loadingTransactionReducer(AppState state, TransactionLoading action) {
  return state.copyWith(transactionLoading : true, status: action.status, paymentErrorMessages: '', requestErrorMessages: '', topupErrorMessages: '');
}

AppState paymentSuccessReducer(AppState state, PaymentSuccess action) {
  return state.copyWith(transactionLoading: false, status: 'paymentSuccess', paymentErrorMessages: '');
}

AppState paymentFailReducer(AppState state, PaymentFailed action) {
  return state.copyWith(transactionLoading: false, status: 'paymentFailed', paymentErrorMessages: action.message);
}

AppState getAmountReducer(AppState state, GetAmount action) {
  return state.copyWith(detailUser: state.detailUser.copyWith(amount: action.amount));
}

AppState transactionSuccesReducer(AppState state, TransactionSucces action){
  return state.copyWith(transactionLoading: false, transactionList: action.list, status: 'loadTransactionSuccess', requestId: '');
}

AppState resetStatusReducer(AppState state, ResetStatus action){
  return state.copyWith(status: '');
}

AppState requestSuccessReducer(AppState state, RequestSuccess action) {
  return state.copyWith(transactionLoading: false, status: 'requestSuccess', requestErrorMessages: '');
}

AppState requestFailReducer(AppState state, RequestFailed action) {
  return state.copyWith(transactionLoading: false, status: 'requestFailed', requestErrorMessages: action.message);
}

AppState loadRequestSuccessReducer(AppState state, LoadRequestSuccess action){
  return state.copyWith(requestList: action.list, requestId: '');
}

AppState payRequestReducer(AppState state, PayRequest action){
  return state.copyWith(requestId: action.data);
}

AppState logoutReducer(AppState state, Logout action){
  return AppState.initialState();
}

AppState getPayeesReducer(AppState state, GetPayees action) {
  return state.copyWith(detailUser: state.detailUser.copyWith(payees: action.payees));
}
