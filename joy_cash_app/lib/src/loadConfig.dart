import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;

Future<String> loadAsset() async {
  String ipClient = await rootBundle.loadString('assets/config/config.json');
  Map result = json.decode(ipClient);
  return result['apiUrl'];
}

