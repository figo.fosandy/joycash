// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:joy_cash_app/app.dart';


import 'package:joy_cash_app/model/app_state.dart';
import 'package:joy_cash_app/redux/reducers/reducers.dart';
import 'package:joy_cash_app/screen/register.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:faker/faker.dart';

void main() {

  group('Register test case', () {
  testWidgets('Registration screen test render', (WidgetTester register) async {
    //build our app and trigger a frame
          final store = Store<AppState>(
          appStateReducer,
          initialState: AppState.initialState(),
          middleware: [thunkMiddleware, LoggingMiddleware.printer()]
  );
    await register.pumpWidget(App(store: store));
    expect(find.byType(RegisterPage), findsOneWidget);
    expect(find.byType(TextFormField), findsNWidgets(3));
    expect(find.text('Email'),findsOneWidget);
    expect(find.text('Password'),findsOneWidget);
    expect(find.text('Retype Password'),findsOneWidget);
  });

  testWidgets('Form can be submitted', (WidgetTester registForm) async {
    //build our app and trigger a frame
          final store = Store<AppState>(
          appStateReducer,
          initialState: AppState.initialState(),
          middleware: [thunkMiddleware, LoggingMiddleware.printer()]
  );
  
  await registForm.pumpWidget(App(store: store));

  final Finder email = find.widgetWithText(TextFormField, 'Email');
  final Finder password = find.widgetWithText(TextFormField, 'Password');
  final Finder retype = find.widgetWithText(TextFormField, 'Retype Password');
  final Finder submit = find.widgetWithText(MaterialButton, 'SIGN UP');

  // expect(find.text('Form submitted'), findsNothing);
  
  var faker = new Faker();

  await registForm.enterText(email, '${faker.internet.userName()}@realuniversity.com');
  await registForm.enterText(password, '12345678');
  await registForm.enterText(retype, '12345678');

  await registForm.tap(submit);
  await registForm.pump();

  //expect(find.text('register success'), findsOneWidget);
});

  //   testWidgets('Check Container wit image asset background', (WidgetTester login) async {
  //   //build our app and trigger a frame

  //         final store = Store<AppState>(
  //         appStateReducer,
  //         initialState: AppState.initialState(),
  //         middleware: [thunkMiddleware, LoggingMiddleware.printer()]
  // );
  //   await login.pumpWidget(App(store: store));

  //   expect(find.byType(Container), findsWidgets);
  // });

  });
}
