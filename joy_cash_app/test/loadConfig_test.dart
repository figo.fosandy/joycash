import 'package:flutter_test/flutter_test.dart';
import 'package:joy_cash_app/src/loadConfig.dart';


void main() {

  group('Config', () {
  test('loading config file for specific key', () async {
    TestWidgetsFlutterBinding.ensureInitialized();
    final loadConfig = await loadAsset();
    expect(loadConfig, 'http://192.168.129.2:3000');
  });

  });
}