# JoyCash - Payment Flatform 

JoyCash is a flatform mobile app for payment

# Getting Started
Change the uri of mongodb server and port for the api service located on directory joy_cash_api/config/config.json and change the uri of api service for app located on joy_cash_app/assets/config/config.json
```sh
$ git clone <this repo>
$ cd joy_cash_api && npm install
$ cd ./joy_cash_app && flutter pub get
```
# Run App 
***
**Run in folder joy_cash_api with command :**
     > npm run dev
     
**Run in folder joy_cash_app with command :**
    > flutter run
 

#  Features!
****
    > User Registrasi
    > Login & Logout
    > user Make a Security Pin
    > Transaction Summary in Dashboard
    > Transaction Detail
    > Transaction History (Filter and Search transaction)
    > Receive and pay 
    > Top Up money
    > add Payee 
## Tools for make a this Application
 - Visual Studio code
 - Android sdk
 - node js 10.0.0
 - phyton28
 - Flutter 
 - Mongo DB
 
## Language programing Used
 - Flutter - Dart language
 - Node js - Hapi
## Development SetUp
before you begin, you should  already downloaded the Android Studio SDK, Flutter and set it up correctly. You can find a guide on how to do this here: https://codelabs.developers.google.com/codelabs/flutter/#0.

# ScreenShoot
![](https://i.imgsafe.org/02/0291c18e45.png)
![](https://i.imgsafe.org/02/02a9bc8aae.jpeg)
![](https://i.imgsafe.org/02/02a9d1fabe.jpeg)
![](https://i.imgsafe.org/02/02a9af2bb4.jpeg)
![](https://i.imgsafe.org/02/02a9d060a6.jpeg)
![](https://i.imgsafe.org/02/02a9da9923.jpeg)
![](https://i.imgsafe.org/02/02a9d960d7.jpeg)
![](https://i.imgsafe.org/02/02a9b60a54.jpeg)
![](https://i.imgsafe.org/02/02a9bd75a0.jpeg)



## Author 
 - Iqbal Hafid - iqbal.hapid@dkatlis.com
 - Figo Fosandy - figo.fosandy@dkatlis.com
 - Rinrin Karmila - rinrin.karmila@dkatalis.com
